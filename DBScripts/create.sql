DROP TABLE episodes;
DROP TABLE seasons;
DROP TABLE shows;
DROP TABLE user;
DROP TABLE subscription;
DROP TABLE episodes_watched;

CREATE TABLE shows (
  show_id INT NOT NULL,
  name VARCHAR(200) NULL,
  overview VARCHAR(10000) NULL,
  image_url VARCHAR(300) NULL,
  PRIMARY KEY (show_id));
  
CREATE TABLE seasons (
  season_id INT NOT NULL,
  show_id INT NULL,
  overview VARCHAR(10000) NULL,
  season_num INT NOT NULL,
  image_url VARCHAR(300) NULL,
  PRIMARY KEY (season_id , show_id));
  
CREATE TABLE episodes (
  episode_id INT NOT NULL,
  season_id INT NOT NULL,
  show_id INT NOT NULL,
  episode_number INT,
  title VARCHAR(50) NULL,
  overview VARCHAR(10000) NULL,
  rating DECIMAL(10) NULL,
  votes INT NULL,
  updated_at DATETIME NULL,
  first_aired DATETIME NULL,
  image_url VARCHAR(300) NULL,
   PRIMARY KEY (episode_id, season_id, show_id));

create table user
(
	username varchar(50) not null,
    firstname varchar(50),
    lastname varchar(50),
    password varchar(50),
    primary key(username)
);

CREATE TABLE subscription (
  userid VARCHAR(50) NOT NULL,
  showid INT NOT NULL,
  PRIMARY KEY (userid, showid)
);

CREATE TABLE episodes_watched (
  username VARCHAR(50) NOT NULL,
  episode_id INT NOT NULL,
  PRIMARY KEY (username, episode_id)
 );
  