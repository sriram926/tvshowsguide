package tvshowsguide.entities;

/**
 * The season id's objects has the season specific id's by Tvdb, Tmdb, Trakt and
 * Tvrage.
 *
 */
public class SeasonIds {

	private Integer tvdb;
	private Integer tmdb;
	private Integer trakt;
	private Integer tvrage;

	public void setTvdb(Integer tvdb) {
		this.tvdb = tvdb;
	}

	public void setTmdb(Integer tmdb) {
		this.tmdb = tmdb;
	}

	public void setTrakt(Integer trakt) {
		this.trakt = trakt;
	}

	public void setTvrage(Integer tvrage) {
		this.tvrage = tvrage;
	}

	public Integer getTvdb() {
		return tvdb;
	}

	public Integer getTmdb() {
		return tmdb;
	}

	public Integer getTrakt() {
		return trakt;
	}

	public Integer getTvrage() {
		return tvrage;
	}

}
