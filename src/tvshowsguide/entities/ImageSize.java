package tvshowsguide.entities;

/**
 * <ul>
 * <li>Type Size Dimensions
 * <li>
 * poster
 * <p>
 * full-1000x1500
 * <p>
 * medium- 600x900
 * <p>
 * thumb -300x450
 * <li>
 * fanart
 * <p>
 * full -1920x1080 (typical), 1280x720
 * <p>
 * medium -1280x720
 * <p>
 * thumb -853x480
 * <li>
 * screenshot
 * <p>
 * full -1920x1080, 1280x720, 400x225 (typical)
 * <p>
 * medium -1280x720
 * <p>
 * thumb -853x480
 * </ul>
 */
public class ImageSize {
	private String full;
	private String medium;
	private String thumb;

	public String getFull() {
		return full;
	}

	public String getMedium() {
		return medium;
	}

	public String getThumb() {
		return thumb;
	}

	public void setFull(String full) {
		this.full = full;
	}

	public void setMedium(String medium) {
		this.medium = medium;
	}

	public void setThumb(String thumb) {
		this.thumb = thumb;
	}

}
