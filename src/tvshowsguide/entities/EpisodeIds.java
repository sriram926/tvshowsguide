package tvshowsguide.entities;

/**
 * The episodes id's objects has the episode specific id's by Tvdb and Tvrage
 *
 */
public class EpisodeIds extends BaseIds {

	private Integer tvdb;
	private Integer tvrage;
	
	public void setTvdb(Integer tvdb) {
		this.tvdb = tvdb;
	}

	public void setTvrage(Integer tvrage) {
		this.tvrage = tvrage;
	}

	public Integer getTvdb() {
		return tvdb;
	}

	public Integer getTvrage() {
		return tvrage;
	}

}
