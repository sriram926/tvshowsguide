package tvshowsguide.entities;

import java.util.List;

/**
 * The base entity has the common fields for all the media objects.
 *
 */
public class BaseEntity {

	private String title;
	private String overview;
	private Double rating;
	private Integer votes;
	private String updated_at;
	private List<String> available_translations;

	public void setTitle(String title) {
		this.title = title;
	}

	public void setOverview(String overview) {
		this.overview = overview;
	}

	public void setRating(Double rating) {
		this.rating = rating;
	}

	public void setVotes(Integer votes) {
		this.votes = votes;
	}

	public void setUpdated_at(String updated_at) {
		this.updated_at = updated_at;
	}

	public void setAvailable_translations(List<String> available_translations) {
		this.available_translations = available_translations;
	}

	public String getTitle() {
		if (title == null) {
			return "Unknown";
		}
		return title;
	}

	public String getOverview() {
		return overview;
	}

	public Double getRating() {
		return rating;
	}

	public Integer getVotes() {
		return votes;
	}

	public String getUpdated_at() {
		return updated_at;
	}

	public List<String> getAvailable_translations() {
		return available_translations;
	}

}