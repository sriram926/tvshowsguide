package tvshowsguide.entities;

/**
 * The base id's objects has the media objects id's by Trakt, Imdb and Tmdb
 *
 */
public abstract class BaseIds {

	public void setTrakt(int trakt) {
		this.trakt = trakt;
	}

	public void setImdb(String imdb) {
		this.imdb = imdb;
	}

	public void setTmdb(int tmdb) {
		this.tmdb = tmdb;
	}

	private int trakt;
	private String imdb;
	private int tmdb;

	public int getTrakt() {
		return trakt;
	}

	public String getImdb() {
		return imdb;
	}

	public int getTmdb() {
		return tmdb;
	}

}
