package tvshowsguide.entities;

/**
 * The airs object is relative to the show's country air time.
 * <p>
 * The fields day, time, and time zone can be used to construct date and convert
 * it to the time zone of the user currently in.
 *
 */
public class Airs {
	private String day;
	private String time;
	private String timezone;

	public String getDay() {
		return day;
	}

	public String getTime() {
		return time;
	}

	public String getTimezone() {
		return timezone;
	}
}
