package tvshowsguide.entities;

/**
 * The show id's objects has the show specific id's by Slug, Tmdb and Tvrage.
 *
 */
public class ShowIds extends BaseIds {

	private String slug;
	private int tvdb;
	private int tvrage;

	public String getSlug() {
		return slug;
	}

	public int getTvdb() {
		return tvdb;
	}

	public int getTvrage() {
		return tvrage;
	}

}
