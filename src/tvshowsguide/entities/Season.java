package tvshowsguide.entities;

/**
 * The season object has a single season for a show including the number of
 * episodes in each season.
 *
 */
public class Season {
	private Integer number;
	private Integer showId;
	public SeasonIds ids = new SeasonIds();
	private String overview;
	private Double rating;
	private Integer votes;
	private Integer episode_count;
	private Integer aired_episodes;
	public Images images = new Images();

	public void setShowId(Integer showId) {
		this.showId = showId;
	}

	public Integer getNumber() {
		return number;
	}

	public Integer getShowId() {
		return showId;
	}

	public SeasonIds getIds() {
		return ids;
	}

	public void setNumber(Integer number) {
		this.number = number;
	}

	public void setIds(SeasonIds ids) {
		this.ids = ids;
	}

	public void setOverview(String overview) {
		this.overview = overview;
	}

	public void setRating(Double rating) {
		this.rating = rating;
	}

	public void setVotes(Integer votes) {
		this.votes = votes;
	}

	public void setEpisode_count(Integer episode_count) {
		this.episode_count = episode_count;
	}

	public void setAired_episodes(Integer aired_episodes) {
		this.aired_episodes = aired_episodes;
	}

	public void setImages(Images images) {
		this.images = images;
	}

	public String getOverview() {
		return overview;
	}

	public Double getRating() {
		return rating;
	}

	public Integer getVotes() {
		return votes;
	}

	public Integer getEpisode_count() {
		return episode_count;
	}

	public Integer getAired_episodes() {
		return aired_episodes;
	}

	public Images getImages() {
		return images;
	}

	public String getName() {
		String seasonName = "";
		if (number == 0) {
			seasonName = "Special Episodes";
		} else {
			seasonName = "Season " + number;
		}
		return seasonName;
	}
}
