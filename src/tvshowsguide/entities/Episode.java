package tvshowsguide.entities;

/**
 * The episodes object has a single episode's details. All date and times are in
 * UTC and were calculated using the episode's air_date and show's country and
 * air_time.
 * <p>
 * Note: If the first_aired is unknown, it will be set to null.
 *
 */
public class Episode extends BaseEntity {

	private Integer season;
	private Integer seasonId;
	private Integer showId;
	private Integer number;
	public EpisodeIds ids = new EpisodeIds();
	private Integer number_abs;
	private String first_aired;
	public Images images = new Images();
	private String showName;

	public String getShowName() {
		return showName;
	}

	public void setShowName(String showName) {
		this.showName = showName;
	}

	public Integer getSeason() {
		return season;
	}

	public void setSeason(Integer season) {
		this.season = season;
	}

	public void setNumber(Integer number) {
		this.number = number;
	}

	public void setIds(EpisodeIds ids) {
		this.ids = ids;
	}

	public void setNumber_abs(Integer number_abs) {
		this.number_abs = number_abs;
	}

	public void setFirst_aired(String first_aired) {
		this.first_aired = first_aired;
	}

	public void setImages(Images images) {
		this.images = images;
	}

	public void setSeasonId(Integer seasonId) {
		this.seasonId = seasonId;
	}

	public void setShowId(Integer showId) {
		this.showId = showId;
	}

	public Integer getSeasonId() {
		return seasonId;
	}

	public Integer getShowId() {
		return showId;
	}

	public Integer getNumber() {
		return number;
	}

	public EpisodeIds getIds() {
		return ids;
	}

	public Integer getNumber_abs() {
		return number_abs;
	}

	public String getFirst_aired() {
		return first_aired;
	}

	public Images getImages() {
		return images;
	}

}
