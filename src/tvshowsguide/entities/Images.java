package tvshowsguide.entities;

/**
 * All images will be returned in their full size. Posters and fanart will have
 * additional smaller sizes.
 * <p>
 * Example:
 * "https://walter.trakt.us/images/movies/000/000/001/posters/original/e0d9dd35c5.jpg?1400478209"
 * 
 * <ul>
 * <li>Type Size Dimensions
 * <li>
 * poster
 * <p>
 * full-1000x1500
 * <p>
 * medium- 600x900
 * <p>
 * thumb -300x450
 * <li>
 * fanart
 * <p>
 * full -1920x1080 (typical), 1280x720
 * <p>
 * medium -1280x720
 * <p>
 * thumb -853x480
 * <li>
 * screenshot
 * <p>
 * full -1920x1080, 1280x720, 400x225 (typical)
 * <p>
 * medium -1280x720
 * <p>
 * thumb -853x480
 * </ul>
 * 
 */
public class Images {
	private ImageSize poster;
	private ImageSize fanart;
	private ImageSize screenshot;

	public void setPoster(ImageSize poster) {
		this.poster = poster;
	}

	public void setFanart(ImageSize fanart) {
		this.fanart = fanart;
	}

	public void setScreenshot(ImageSize screenshot) {
		this.screenshot = screenshot;
	}

	public ImageSize getPoster() {
		return poster;
	}

	public ImageSize getFanart() {
		return fanart;
	}

	public ImageSize getScreenshot() {
		return screenshot;
	}
}