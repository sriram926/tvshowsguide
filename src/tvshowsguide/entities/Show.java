package tvshowsguide.entities;

import java.util.ArrayList;
import java.util.List;

/**
 * The show object has a single shows's details. It has extended info, the
 * airs object is relative to the show's country. You can use the day, time, and
 * time zone to construct your own date then convert it to whatever time zone
 * your user is in.The status field can have a value of returning series (airing
 * right now), in production (airing soon), canceled, or ended.
 *
 */
public class Show extends BaseEntity implements Media {

	private Integer year;
	private Airs air = new Airs();
	private String first_aired;
	private Integer runtime;
	private String certification;
	private String network;
	private String country;
	private String language;
	private List<String> genres = new ArrayList<String>();
	public ShowIds ids = new ShowIds();
	public Images images = new Images();
	private String status;

	public Show() {
	}

	public void setYear(Integer year) {
		this.year = year;
	}

	public void setAir(Airs air) {
		this.air = air;
	}

	public void setFirst_aired(String first_aired) {
		this.first_aired = first_aired;
	}

	public void setRuntime(Integer runtime) {
		this.runtime = runtime;
	}

	public void setCertification(String certification) {
		this.certification = certification;
	}

	public void setNetwork(String network) {
		this.network = network;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public void setGenres(List<String> genres) {
		this.genres = genres;
	}

	public void setIds(ShowIds ids) {
		this.ids = ids;
	}

	public void setImages(Images images) {
		this.images = images;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Integer getYear() {
		return year;
	}

	public Airs getAir() {
		return air;
	}

	public String getFirst_aired() {
		return first_aired;
	}

	public Integer getRuntime() {
		return runtime;
	}

	public String getCertification() {
		return certification;
	}

	public String getNetwork() {
		return network;
	}

	public String getCountry() {
		return country;
	}

	public String getLanguage() {
		return language;
	}

	public List<String> getGenres() {
		return genres;
	}

	public ShowIds getIds() {
		return ids;
	}

	public Images getImages() {
		return images;
	}
	
	public String getStatus() {
		return status;
	}
}
