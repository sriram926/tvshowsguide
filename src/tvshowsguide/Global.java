package tvshowsguide;

import java.awt.Color;
import javax.swing.JButton;
import tvshowsguide.api.Pagination;
import tvshowsguide.gui.MainFrame;

/**
 * Global is a singleton class where all the application properties are set.
 * <p>
 * Sets all the application icons for buttons Add, Remove, Details, User, Back,
 * Default Image, Close, Clock, Search
 * <p>
 * Sets number of look up days for upcoming and recent episodes.
 * <p>
 * Sets the pagination for trending and search results.
 * <p>
 * Sets the primary and secondary colors for the application.
 * 
 */
public class Global {
	private static Global instance = new Global();

	private static int subscriptionstate = 99;
	private static MainFrame mainFrame = new MainFrame();

	public static final String ICON_ADD = "images/icon_add.png";
	public static final String ICON_REMOVE = "images/icon_remove.png";
	public static final String ICON_DETAILS = "images/icon_details.png";
	public static final String ICON_USER = "images/icon_user.png";
	public static final String ICON_BACK = "images/icon_back.png";
	public static final String ICON_NULL = "images/icon_null.png";
	public static final String ICON_CLOSE = "images/icon_close.png";
	public static final String ICON_CLOCK = "images/icon_clock.png";
	public static final String ICON_SEARCH = "images/icon_search.png";

	public static final int LOOKUPDAYS_UPCOMING = 7;
	public static final int LOOKUPDAYS_RECENT = 7;
	public static Pagination PAGINATION_TRENDING = new Pagination(1, 5);
	public static Pagination PAGINATION_SEARCH = new Pagination(1, 5);
	public static boolean ENABLE_NOTIFICATIONS = true;

	public Color uicolor_primary = new Color(98, 100, 105);
	public Color uicolor_secondary = new Color(25, 25, 25);

	private Global() {
	}

	/**
	 * 
	 * @return instance of Global Class
	 */
	public static Global getInstance() {
		return instance;
	}

	/**
	 * Gets the current main frame
	 * 
	 * @return MainFrame
	 */
	public static MainFrame getMainFrame() {
		return mainFrame;
	}

	/**
	 * Gets the current subscription state
	 * <p>
	 * subscription state = 99 default
	 * <p>
	 * subscription state = 1 if user subscribed shows changes.
	 * <p>
	 * subscription state = 0 if the application is refreshed for current user
	 * subscribed shows changes.
	 * 
	 * @return subscription state
	 */
	public static int getSubscriptionstate() {
		return subscriptionstate;
	}

	/**
	 * Sets the current subscription state
	 * <p>
	 * subscription state = 99 default
	 * <p>
	 * subscription state = 1 if user subscribed shows changes.
	 * <p>
	 * subscription state = 0 if the application is refreshed for current user
	 * subscribed shows changes.
	 */
	public static void setSubscriptionstate(int subscripbedstate) {
		Global.subscriptionstate = subscripbedstate;
	}

	/**
	 * Creates JButton with properties
	 * <p>
	 * setOpaque = false
	 * <p>
	 * setContentAreaFilled = false
	 * <p>
	 * setBorderPainted = false
	 * <p>
	 * setFocusPainted = false
	 * 
	 * @return new JButton
	 * @see JButton
	 */
	public static JButton createButton() {
		JButton btn = new JButton();
		btn.setOpaque(false);
		btn.setContentAreaFilled(false);
		btn.setBorderPainted(false);
		btn.setFocusPainted(false);
		return btn;
	}
}
