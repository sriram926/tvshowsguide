package tvshowsguide.api;

import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import com.google.gson.Gson;
import tvshowsguide.entities.*;
import tvshowsguide.enums.Extended;
import tvshowsguide.enums.MediaType;
import tvshowsguide.enums.TraktURIs;

/**
 * Makes requests to the API
 * <p>
 * Has methods to
 * <p>
 * Perform a text query that searches titles, descriptions, translated titles,
 * aliases, and people. Items searched include movies, shows, episodes, people,
 * and lists. Results are ordered by the most relevant score.
 * <p>
 * Given the id gets the media summary.
 * <p>
 * Gets most popular and trending media.
 * <p>
 * Can set pagination as required to return results.
 * 
 */
public class ApiHelper {
	private TraktApi traktapi = new TraktApi();
	private Pagination pagination = new Pagination(1, 1);

	public ApiHelper() {
	}

	public ApiHelper(Pagination pagination) {
		this.pagination = pagination;
	}

	/**
	 * Perform a text query that searches titles, descriptions, translated
	 * titles, aliases, and people. Items searched include movies, shows,
	 * episodes, people, and lists. Results are ordered by the most relevant
	 * score.
	 * 
	 * @param query Searches titles and descriptions. Example: batman.
	 * @param mediatype Narrows down search by media type.
	 * @return List of media objects
	 */
	public List<Media> getMedia(String query, MediaType mediatype) {
		List<QueryParam> queryParams = new ArrayList<QueryParam>();
		QueryParam queryParam = new QueryParam("query", query);
		queryParams.add(queryParam);
		queryParam = new QueryParam("type", mediatype.toString());
		queryParams.add(queryParam);
		queryParams.addAll(pagination.getParams());
		return searchMedia(queryParams);
	}

	/**
	 * Perform a text query that searches titles, descriptions, translated
	 * titles, aliases, and people. Items searched include movies, shows,
	 * episodes, people, and lists. Results are ordered by the most relevant
	 * score.
	 * 
	 * You can filter the results by the year of the movie and show.
	 * 
	 * @param query Searches titles and descriptions. Example: batman.
	 * @param mediatype Narrows down search by media type.
	 * @param year Limits results to a specific year. Example: 2015.
	 * @return List of media objects
	 */
	public List<Media> getMedia(String query, MediaType mediatype, int year) {
		List<QueryParam> queryParams = new ArrayList<QueryParam>();
		QueryParam queryParam = new QueryParam("query", query);
		queryParams.add(queryParam);
		queryParam = new QueryParam("type", mediatype.toString());
		queryParams.add(queryParam);
		queryParam = new QueryParam("year", Integer.toBinaryString(year));
		queryParams.add(queryParam);
		queryParams.addAll(pagination.getParams());
		return searchMedia(queryParams);
	}

	/**
	 * Perform a text query that searches titles, descriptions, translated
	 * titles, aliases, and people. Items searched include movies, shows,
	 * episodes, people, and lists. Results are ordered by the most relevant
	 * score.
	 * 
	 * You can optionally limit the type of results to return. Send a comma
	 * separated string to get results for multiple types. You can optionally
	 * filter the year for movie and show results.
	 * 
	 * @param queryParams
	 * @return List of media objects
	 */
	private List<Media> searchMedia(List<QueryParam> queryParams) {
		RequestPath requestpath = new RequestPath(TraktURIs.SEARCH.toString());
		List<Media> media = new ArrayList<Media>();
		QueryParam queryParam = new QueryParam("extended", Extended.FULLIMAGES.toString());
		queryParams.add(queryParam);
		traktapi.getResponse(requestpath, queryParams);
		Gson gson = new Gson();
		try {
			JSONArray jsonArray = new JSONArray(traktapi.responseBody);
			for (int i = 0; i < jsonArray.length(); i++) {
				JSONObject jsonObject = jsonArray.getJSONObject(i);
				String type = jsonObject.getString("type");
				String jsonstr = jsonObject.getJSONObject(type).toString();
				Show show = gson.fromJson(jsonstr, Show.class);
				media.add(show);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return media;
	}

	/**
	 * Returns a single shows's details.
	 * 
	 * @param showid trakt ID, trakt slug, or IMDB ID Example: game-of-thrones
	 * @param extended returns different levels of information.
	 * @return Show object
	 */
	public Show getShowSummary(String showid, Extended extended) {
		RequestPath requestpath = new RequestPath(TraktURIs.SHOW_SUMMARY.toString(), showid);
		List<QueryParam> queryParams = new ArrayList<QueryParam>();
		QueryParam queryParam = new QueryParam("extended", extended.toString());
		queryParams.add(queryParam);
		traktapi.getResponse(requestpath, queryParams);
		Gson gson = new Gson();
		return gson.fromJson(traktapi.responseBody, Show.class);
	}

	/**
	 * 
	 * Returns all seasons for a show including the number of episodes in each
	 * season.
	 * 
	 * @param showid trakt ID, trakt slug, or IMDB ID Example: game-of-thrones
	 * @param extended returns different levels of information.
	 * @return List of Season objects
	 */
	public List<Season> getShowSeasons(String showid, Extended extended) {
		List<Season> seasons = new ArrayList<Season>();
		RequestPath requestpath = new RequestPath(TraktURIs.SHOW_SEASONS.toString(), showid);
		List<QueryParam> queryParams = new ArrayList<QueryParam>();
		QueryParam queryParam = new QueryParam("extended", extended.toString());
		queryParams.add(queryParam);
		traktapi.getResponse(requestpath, queryParams);
		Gson gson = new Gson();
		try {
			JSONArray jsonArray = new JSONArray(traktapi.responseBody);
			for (int i = 0; i < jsonArray.length(); i++) {
				String jsonObject = jsonArray.getJSONObject(i).toString();
				Season season = gson.fromJson(jsonObject, Season.class);
				seasons.add(season);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return seasons;
	}

	/**
	 * Returns all episodes for a specific season of a show.
	 * 
	 * @param showid trakt ID, trakt slug, or IMDB ID Example: game-of-thrones
	 * @param seasonnumber season number Example: 1.
	 * @param extended returns different levels of information.
	 * @return List of Episode objects
	 */
	public List<Episode> getSeasonEpisodes(String showid, String seasonnumber, Extended extended) {
		List<Episode> episodes = new ArrayList<Episode>();
		RequestPath requestpath = new RequestPath(TraktURIs.SEASON_EPISODES.toString(), showid, seasonnumber);
		List<QueryParam> queryParams = new ArrayList<QueryParam>();
		QueryParam queryParam = new QueryParam("extended", extended.toString());
		queryParams.add(queryParam);
		traktapi.getResponse(requestpath, queryParams);
		Gson gson = new Gson();
		try {
			JSONArray jsonArray = new JSONArray(traktapi.responseBody);
			for (int i = 0; i < jsonArray.length(); i++) {
				String jsonObject = jsonArray.getJSONObject(i).toString();
				Episode episode = gson.fromJson(jsonObject, Episode.class);
				episodes.add(episode);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return episodes;
	}

	/**
	 * Returns the most popular shows. Popularity is calculated using the rating
	 * percentage and the number of ratings.
	 * 
	 * @return List of Show objects
	 */
	public List<Show> getPopularShows() {
		RequestPath requestpath = new RequestPath(TraktURIs.POPULAR_SHOWS.toString());
		return getShows(requestpath);
	}

	/**
	 * Returns all shows being watched right now. Shows with the most users are
	 * returned first.
	 * 
	 * @return List of Show objects
	 */
	public List<Show> getTrendingShows() {
		RequestPath requestpath = new RequestPath(TraktURIs.TRENDING_SHOWS.toString());
		return getShows(requestpath);
	}

	/**
	 * Returns shows list based on the given request path.
	 * 
	 * @param requestpath
	 * @return List of Show objects
	 */
	private List<Show> getShows(RequestPath requestpath) {
		List<Show> shows = new ArrayList<Show>();
		List<QueryParam> queryParams = new ArrayList<QueryParam>();
		QueryParam queryParam = new QueryParam("extended", Extended.FULLIMAGES.toString());
		queryParams.add(queryParam);
		queryParams.addAll(pagination.getParams());
		traktapi.getResponse(requestpath, queryParams);
		Gson gson = new Gson();
		try {
			JSONArray jsonArray = new JSONArray(traktapi.responseBody);
			for (int i = 0; i < jsonArray.length(); i++) {
				JSONObject jsonObject = jsonArray.getJSONObject(i);
				String jsonstr = jsonObject.getJSONObject("show").toString();
				Show show = gson.fromJson(jsonstr, Show.class);
				shows.add(show);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return shows;
	}

	public void setPagination(Pagination pagination) {
		this.pagination = pagination;
	}
}
