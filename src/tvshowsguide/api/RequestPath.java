package tvshowsguide.api;

/**
 * Generates URL for making an API call.
 * <p>
 * Can set the request path and its path parameters.
 *
 */
public class RequestPath {
	private String requestpath;

	/**
	 * Set the request path for API Call.
	 * 
	 * @param requestpath
	 */
	public RequestPath(String requestpath) {
		this.requestpath = requestpath;
	}

	/**
	 * Set the request path and replaces its path parameters with the gives
	 * objects.
	 * 
	 * @param requestpath
	 * @param objects
	 */
	public RequestPath(String requestpath, Object... objects) {
		this.requestpath = requestpath;
		for (int i = 0; i < objects.length; i++) {
			this.requestpath = this.requestpath.replaceAll("\\{" + i + "\\}", (String) objects[i]);
		}
	}

	/**
	 * Gets the request path set to make the API Call.
	 * 
	 * @return API request path
	 */
	public String get() {
		return requestpath;
	}
}
