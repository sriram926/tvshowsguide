package tvshowsguide.api;

import java.util.ArrayList;
import java.util.List;

/**
 * Use Pagination objects to set parameters number pages and limit objects per
 * page returned from the API
 * 
 */
public class Pagination {
	private int page;
	private int limit;

	/**
	 * Set Pagination parameters page and limit
	 * <p>
	 * Number of page of results to be returned
	 * <p>
	 * Number of results to return per page.
	 * 
	 * @param page
	 * @param limit
	 */
	public Pagination(int page, int limit) {
		this.page = page;
		this.limit = limit;
	}

	/**
	 * Get list of Pagination query parameters.
	 * 
	 * @return
	 */
	public List<QueryParam> getParams() {
		List<QueryParam> queryParams = new ArrayList<QueryParam>();
		queryParams.add(new QueryParam("page", Integer.toString(page)));
		queryParams.add(new QueryParam("limit", Integer.toString(limit)));
		return queryParams;
	}

}
