package tvshowsguide.api;

/**
 * API calls may need some query parameters to be sent. We can create the
 * parameters using this class.
 *
 */
public class QueryParam {
	public String param;
	public String value;

	/**
	 * Creates an QueryParam object with parameter name and its value.
	 * 
	 * @param param
	 * @param value
	 */
	public QueryParam(String param, String value) {
		this.param = param;
		this.value = value;
	}
}
