package tvshowsguide.api;

import java.util.List;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation.Builder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.MediaType;

import tvshowsguide.enums.TraktURIs;

/**
 * This class provides access to Trakt.tv,which has a collection of lots of
 * interesting information about what TV shows and movies everyone is watching.
 * This data is available for anyone to mash up and use.
 * <p>
 * We make API calls by a URL and get some JSON back.
 * <p>
 * We can obtain an authorization code and make API calls such as requesting
 * information about a movie or show by making specific calls based on the
 * request path and request specific query parameters.
 * <p>
 * Set the API request response to String variables
 * <p>
 * Response Status is Set responseStatus
 * <p>
 * <ul>
 * <li>200 Success
 * <li>201 Success - new resource created (POST)
 * <li>204 Success - no content to return (DELETE)
 * <li>400 Bad Request - request couldn't be parsed
 * <li>401 Unauthorized - OAuth must be provided
 * <li>403 Forbidden - invalid API key or unapproved app
 * <li>404 Not Found - method exists, but no record found
 * <li>405 Method Not Found - method doesn't exist
 * <li>409 Conflict - resource already created
 * <li>412 Precondition Failed - use application/json content type
 * <li>422 Unprocessable Entity - validation errors
 * <li>429 Rate Limit Exceeded
 * <li>500 Server Error
 * <li>503 Service Unavailable - server overloaded
 * <li>520 Service Unavailable - Cloudflare error
 * <li>521 Service Unavailable - Cloudflare error
 * <li>522 Service Unavailable - Cloudflare error
 * </ul>
 * <p>
 * Response Headers is Set responseHeaders
 * <p>
 * Response Body is Set responseBody
 */

public class TraktApi {

	private static final MediaType MEDIA_TYPE = MediaType.APPLICATION_JSON_TYPE;
	private static final String HEADER_TRAKT_API_KEY = "trakt-api-key";
	private static final String HEADER_TRAKT_API_VERSION = "trakt-api-version";

	private static final String TRAKT_API_URL = "https://api-v2launch.trakt.tv";

	private static final String TRAKT_API_VERSION = "2";
	private static final String TRAKT_CLIENT_ID = "9be2ee84f484b1bcfe79948d39c0153e03bde852f393910e338fc93bb925eab3";
	private static final String TRAKT_CLIENT_SECRET = "a2344250a8eac67b1a9e5429766c6e07c6adfb157b100fe8c891110ceb1f8ee8";
	private static final String TRAKT_REDIRECT_URI = "urn:ietf:wg:oauth:2.0:oob";

	public String responseStatus;
	public String responseHeaders;
	public String responseBody;

	/**
	 * To obtain an authorization code.
	 *
	 * Send the user to the location URI of this request. Once the user
	 * authorized your app, the server will redirect to redirectUri with the
	 * authorization code and the sent state in the query parameter code.
	 *
	 */
	public void getAuthorizationRequest() {
		Client client = ClientBuilder.newClient();
		WebTarget wt = client.target(TRAKT_API_URL).path(TraktURIs.ACCESS_AUTHORIZE.toString());
		wt = wt.queryParam("response_type", "code");
		wt = wt.queryParam("client_id", TRAKT_CLIENT_ID);
		wt = wt.queryParam("redirect_uri", TRAKT_REDIRECT_URI);
		setResponse(wt.request(MEDIA_TYPE).get());
	}

	/**
	 * The grant is based on an authorization code that was obtained from an
	 * authorization request.
	 *
	 * @param authcode obtained authorization code.
	 */
	public void getAccessTokenRequest(String authcode) {
		Client client = ClientBuilder.newClient();
		WebTarget webtarget = client.target(TRAKT_API_URL).path(TraktURIs.ACCESS_TOKEN.toString());
		Builder builder = webtarget.request(MEDIA_TYPE);
		Response response = builder.post(getTokenPayLoad(authcode, "authorization_code"));
		responseStatus = Integer.toString(response.getStatus());
	}

	/**
	 * Using the authorization code Using parameters redirect_uri to get an
	 * access_token to build the request pay load.
	 * 
	 * @param authcode
	 * @param granttype
	 * @return Request Pay Load
	 */
	private Entity<String> getTokenPayLoad(String authcode, String granttype) {
		String strpayload = "{  'code': '" + authcode + "',  ";
		strpayload += "'client_id': '" + TRAKT_CLIENT_ID + "',  ";
		strpayload += "'client_secret': '" + TRAKT_CLIENT_SECRET + "',  ";
		strpayload += "'redirect_uri': '" + TRAKT_REDIRECT_URI + "',  ";
		strpayload += "'grant_type': '" + granttype + "'}";
		return Entity.json(strpayload);
	}

	/**
	 * API calls such as requesting information about a movie or show by making
	 * specific calls based on the request path and request specific query
	 * parameters.
	 * 
	 * @param requestpath
	 * @param queryParams
	 */
	public void getResponse(RequestPath requestpath, List<QueryParam> queryParams) {
		Client client = ClientBuilder.newClient();
		WebTarget webtarget = client.target(TRAKT_API_URL).path(requestpath.get());
		for (QueryParam queryParam : queryParams) {
			webtarget = webtarget.queryParam(queryParam.param, queryParam.value);
		}
		setResponse(buildRequest(webtarget).get());
	}

	/**
	 * API calls such as requesting information about a movie or show by making
	 * specific calls based on the request path.
	 * 
	 * @param requestpath
	 */
	public void getResponse(RequestPath requestpath) {
		Client client = ClientBuilder.newClient();
		WebTarget webtarget = client.target(TRAKT_API_URL).path(requestpath.get());
		setResponse(buildRequest(webtarget).get());
	}

	/**
	 * Builds the Request with API Required headers for given web target. Need
	 * to send headers when making API calls to identify your application and
	 * set the content type to JSON.
	 * 
	 * @param webtarget
	 * @return
	 */
	private Builder buildRequest(WebTarget webtarget) {
		Builder builder = webtarget.request(MEDIA_TYPE);
		builder = builder.header(HEADER_TRAKT_API_VERSION, TRAKT_API_VERSION);
		builder = builder.header(HEADER_TRAKT_API_KEY, TRAKT_CLIENT_ID);
		return builder;
	}

	/**
	 * Set response outputs to the parameters
	 * 
	 * @param response
	 */
	private void setResponse(Response response) {
		responseStatus = Integer.toString(response.getStatus());
		responseHeaders = response.getHeaders().toString();
		responseBody = response.readEntity(String.class);
	}
}
