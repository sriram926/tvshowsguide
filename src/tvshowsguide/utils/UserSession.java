package tvshowsguide.utils;

/**
 * This class is a singleton class to hold the user details of the logged in the
 * application.
 *
 */
public class UserSession {
	private static String userId = null;
	private static UserSession us = null;

	private UserSession() {
	}

	/**
	 * Creates the user Session
	 * 
	 * @param user id
	 */
	public static void createSession(String id) {
		if (us == null) {
			us = new UserSession();
			UserSession.userId = id;
		}
	}

	/**
	 * Gets the user id of the current logged in user.
	 * 
	 */
	public static String getUserid() {
		return UserSession.userId;
	}
}
