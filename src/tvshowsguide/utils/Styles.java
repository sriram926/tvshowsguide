package tvshowsguide.utils;

import java.awt.Font;

/**
 * Can set the font family, style, and size for entire application.
 *
 */
public class Styles {

	/**
	 * Gets the font family, style, and size for Titles in the application
	 * 
	 * @return Font for Tiles
	 */
	public static Font getTitleFont() {
		return new Font("Verdana", 1, 20);
	}

	/**
	 * Gets the font family, style, and size for information in the application
	 * 
	 * @return Font for information
	 */
	public static Font getInfoFont() {
		return new Font("Verdana", 1, 16);
	}

}
