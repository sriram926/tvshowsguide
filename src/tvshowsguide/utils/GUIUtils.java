package tvshowsguide.utils;

import java.awt.Color;
import java.awt.Image;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.Border;

import tvshowsguide.Global;

/**
 * This class can be used to get JLabels with ImageIcon set with given image
 * from the system or a web URL.
 * <p>
 * Can validate JTextFields for empty fields and indicate it with border color
 * change.
 * <p>
 * Can compare two JTextFields and indicate it with border color change.
 *
 */
public class GUIUtils {

	/**
	 * Returns a JLabel object with an image icon that can then be painted on
	 * the screen. The URL argument must specify an absolute {@link URL}. The
	 * image icon is scaled to the default width(150) and height(200).
	 * 
	 * @param imageURL an absolute URL giving the base location of the image
	 * @return JLabel object with an image icon at the specified URL
	 * @see JLabel
	 */
	public static JLabel getImageFromURL(String imageURL) {
		return getImageFromURL(imageURL, 150, 200);
	}

	/**
	 * Returns a JLabel object with an image icon that can then be painted on
	 * the screen. The URL argument must specify an absolute {@link URL}. The
	 * image icon is scaled to the specified width and height.
	 * 
	 * @param imageURL an absolute URL giving the base location of the image
	 * @param width required width of the image
	 * @param height required height of the image
	 * @return JLabel object with an image icon at the specified URL with
	 *         specified width and height
	 * @see JLabel
	 */
	public static JLabel getImageFromURL(String imageURL, int width, int height) {
		if (imageURL == null) {
			return getImageFromPath(Global.ICON_NULL, width, height);
		}
		Image image = null;
		try {
			URL url = new URL(imageURL);
			HttpURLConnection con = (HttpURLConnection) url.openConnection();
			con.addRequestProperty("User-Agent", "Mozilla/4.76");
			image = ImageIO.read(con.getInputStream());
			if (image == null) {
				return getImageFromPath(Global.ICON_NULL, width, height);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		ImageIcon icon = new ImageIcon(image);
		JLabel label = new JLabel(icon);
		Image scaledImg = icon.getImage().getScaledInstance(width, height, Image.SCALE_SMOOTH);
		label = new JLabel(new ImageIcon(scaledImg));
		return label;
	}

	/**
	 * Returns a JLabel object with an image icon that can then be painted on
	 * the screen. The imageName argument must specify the name of the image
	 * with folder from the project source. The image icon is scaled to the
	 * default width(125) and height(200).
	 * 
	 * @param imageName location of the image with the name of the image
	 *        Separated by '/'
	 * @return JLabel object with an image icon at the specified name with
	 *         location
	 * @see JLabel
	 */
	public static JLabel getImageFromPath(String imageName) {
		return getImageFromPath(imageName, 125, 200);
	}

	/**
	 * Returns a JLabel object with an image icon that can then be painted on
	 * the screen. The imageName argument must specify the name of the image
	 * with folder from the project source. The image icon is scaled to the
	 * specified width and height.
	 * 
	 * @param imageName location of the image with the name of the image
	 *        Separated by '/'
	 * @param width required width of the image
	 * @param height required height of the image
	 * @return JLabel object with an image icon at the specified name with
	 *         location with specified width and height
	 * @see JLabel
	 */
	public static JLabel getImageFromPath(String imageName, int width, int height) {
		ImageIcon icon = new ImageIcon(imageName);
		JLabel label = new JLabel(icon);

		Image scaledImg = icon.getImage().getScaledInstance(width, height, Image.SCALE_SMOOTH);
		label = new JLabel(new ImageIcon(scaledImg));
		return label;
	}

	/**
	 * Returns a ImageIcon object with an image icon that can then be painted on
	 * the screen. The imageName argument must specify the name of the image
	 * with folder from the project source. The image icon is scaled to the
	 * specified width and height.
	 * 
	 * @param imageName location of the image with the name of the image
	 *        Separated by '/'
	 * @param width required width of the image
	 * @param height required height of the image
	 * @return ImageIcon object with an image at the specified name with
	 *         location with specified width and height
	 * @see ImageIcon
	 */
	public static ImageIcon getImageIconFromPath(String imageName, int width, int height) {
		ImageIcon icon = new ImageIcon(imageName);
		Image scaledImg = icon.getImage().getScaledInstance(width, height, Image.SCALE_SMOOTH);
		return new ImageIcon(scaledImg);
	}

	/**
	 * Set the layout of the argument JPanel main container to have a grid and
	 * adds panels to the main container based no the number of rows and columns
	 * to return a JPanel Two Dimensional array.
	 * 
	 * @param rows number of rows in the grid.
	 * @param cols number of columns in the grid.
	 * @param maincontainer JPanel object to which the gird layout needed to be
	 *        set.
	 * @return Two Dimensional array of JPanel as they are added to the grid.
	 * @see JPanel
	 */
	public static JPanel[][] addGridPanels(int rows, int cols, JPanel maincontainer) {
		JPanel[][] holders = new JPanel[rows][cols];
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < cols; j++) {
				holders[i][j] = new JPanel();
				holders[i][j].setBorder(BorderFactory.createLineBorder(Color.red));
				maincontainer.add(holders[i][j]);
			}
		}
		return holders;
	}

	/**
	 * Checks if the given text field is empty or not. If the text field is
	 * empty return true and set the text fields border to red.
	 * 
	 * @param jTextFields JTextField as arguments which are can not be empty.
	 * @return True is any one of the JTextField text is empty and set the
	 *         JTextField border to red.
	 * @see JTextField
	 */
	public static boolean requiredFields(JTextField... jTextFields) {
		boolean retval = true;
		Border border = BorderFactory.createLineBorder(Color.RED, 1);
		Border borderDefault = BorderFactory.createLineBorder(new Color(171, 173, 179), 1);
		for (JTextField jTextField : jTextFields) {
			if (jTextField.getText().equals("")) {
				jTextField.setBorder(border);
				retval = false;
			} else {
				jTextField.setBorder(borderDefault);
			}
		}
		return retval;
	}

	/**
	 * Compare to JTextFields to check if both have the same text. Returns true
	 * if they match and false if they do not match and sets the border of the
	 * JTextField to orange.
	 * 
	 * @param jTextField1 JTextField to compare with.
	 * @param jTextField2 JTextField to compare with.
	 * @return Returns true if they match and false if they do not match and
	 *         sets the border of the JTextField to orange.
	 * @see JTextField
	 */
	public static boolean compareFields(JTextField jTextField1, JTextField jTextField2) {
		boolean retval = true;
		Border border = BorderFactory.createLineBorder(Color.ORANGE, 1);
		if (!jTextField1.getText().equals(jTextField2.getText())) {
			jTextField1.setBorder(border);
			jTextField2.setBorder(border);
			retval = false;
		}
		return retval;
	}
}
