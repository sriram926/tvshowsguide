package tvshowsguide.utils;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Can be used to convert the date strings to required format.
 * <p>
 * Can covert given date with respect to the current time in terms of days ago
 * or days in or NOW
 *
 */
public class DateUtils {

	private static int TIME_THRESHOLD = 4;

	/**
	 * Converts date in the ISO 8601 format like 2014-09-01T09:10:11.000Z to SQL
	 * Timestamp
	 * 
	 * @param isoDate ISO 8601 format like 2014-09-01T09:10:11.000Z
	 * @return date in format of SQL Timestamp
	 * @see java.sql.Timestamp
	 */
	public static Timestamp convertISOToSQL(String isoDate) {
		if (isoDate == null)
			return null;
		Calendar c = javax.xml.bind.DatatypeConverter.parseDateTime(isoDate);
		Date utilDate = c.getTime();
		java.sql.Timestamp timestamp = new java.sql.Timestamp(utilDate.getTime());
		return timestamp;
	}

	/**
	 * Converts date string to the "yyyy-MM-dd HH:mm:SS".
	 * <p>
	 * Function returns in terms of days ago if given date is less than current
	 * date or hours ago if the dates are same but time is less than the current
	 * time
	 * <p>
	 * Function returns in terms of days in if given date is greater than
	 * current date or hours in if the dates are same but time is greater than
	 * the current time
	 * <p>
	 * Function returns NOW if given date is equal to current and time is equal
	 * to the current time.
	 * 
	 * @param dateString
	 * 
	 * @return returns in terms of days ago if given date is less than current
	 *         date or hours ago if the dates are same but time is less than the
	 *         current time
	 *         <p>
	 *         returns in terms of days in if given date is greater than current
	 *         date or hours in if the dates are same but time is greater than
	 *         the current time
	 *         <p>
	 *         returns NOW if given date is equal to current and time is equal
	 *         to the current time.
	 */
	public static String formatDate(String dateString) {
		if ((dateString == null)) {
			return "Unknown";
		}
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:SS");
		Date date1 = null;
		try {
			date1 = formatter.parse(dateString);
			formatter.applyPattern("EE. MMM. dd, h:mm a yyyy");
		} catch (ParseException e) {
			e.printStackTrace();
		}
		Calendar cal1 = Calendar.getInstance();
		Date curr_date = cal1.getTime();
		String isClose = getRelativeTime(date1, curr_date);
		return isClose == null ? formatter.format(date1) : isClose;
	}

	/**
	 * Function returns in terms of days ago if given date1 is less than date2
	 * or hours ago if the dates are same but date1 time is less than the date2
	 * time.
	 * <p>
	 * Function returns in terms of days in if given date1 is greater than date2
	 * or hours in if the dates are same by date1 time is greater than the date2
	 * time.
	 * <p>
	 * Function returns NOW if given date1 is equal to date2 and date1 time is
	 * equal to the date2 time.
	 * 
	 * @param d1 date1
	 * @param d2 date2
	 * @return dateString returns in terms of days ago if given date1 is less
	 *         than date2 or hours ago if the dates are same but date1 time is
	 *         less than the date2 time
	 *         <p>
	 *         returns in terms of days in if given date1 is greater than date2
	 *         or hours in if the dates are same but date1 time is greater than
	 *         the date2 time
	 *         <p>
	 *         returns NOW if given date1 is equal to date2 and date1 time is
	 *         equal to the date2 time
	 */
	private static String getRelativeTime(Date d1, Date d2) {
		long diff = d2.getTime() - d1.getTime();
		long diffSeconds = diff / 1000 % 60;
		long diffMinutes = diff / (60 * 1000) % 60;
		long diffHours = diff / (60 * 60 * 1000);
		int diffInDays = (int) diff / (1000 * 60 * 60 * 24);

		String msgString;
		if (Math.abs(diffInDays) > TIME_THRESHOLD) {
			return null;
		}
		if (Math.abs(diffInDays) > 0) {
			msgString = Integer.toString(Math.abs(diffInDays)) + " days";
		} else if (Math.abs(diffHours) > 0) {
			msgString = Math.abs(diffHours) + " hours";
		} else if (Math.abs(diffMinutes) > 0) {
			msgString = Math.abs(diffMinutes) + " minutes";
		} else {
			return "NOW";
		}
		return diffSeconds < 0 ? "In " + msgString : msgString + " ago";
	}
}
