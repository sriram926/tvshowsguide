package tvshowsguide.utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import tvshowsguide.exceptions.DBException;

/**
 * This class can be used to execute queries with the current database
 * connection by Statement or by PreparedStatement.
 * 
 * @see java.sql.Statement
 * @see PreparedStatement
 */
public class DBUtils {

	/**
	 * Executes the given query and returns the ResultSet.
	 * 
	 * @param query
	 * @return ResultSet
	 * @throws DBException
	 * @see ResultSet
	 */
	public ResultSet executeSelectQuery(String query) throws DBException {
		try {
			Connection conn = DBConnection.getConnection();
			java.sql.Statement st = conn.createStatement();
			ResultSet rs = st.executeQuery(query);
			return rs;
		} catch (DBException | SQLException e) {
			throw new DBException("Unable to execute query.", e);
		}
	}

	/**
	 * Generates PreparedStatement with the given query.
	 * 
	 * @param query
	 * @return ResultSet
	 * @throws DBException
	 */
	public PreparedStatement getPreparedStatement(String query) throws DBException {
		try {
			Connection conn = DBConnection.getConnection();
			PreparedStatement ps = conn.prepareStatement(query);
			return ps;
		} catch (DBException | SQLException e) {
			throw new DBException("Unable to execute query.", e);
		}
	}

}

/**
 * DBConnection is a singleton class which can be used to get the connection
 * object of the database.
 *
 */
class DBConnection {
	private static String driver = "com.mysql.jdbc.Driver";
	private static String connection = "jdbc:mysql://141.209.169.223:3306/tvguide";
	private static String user = "root";
	private static String password = "support";
	private static Connection conn = null;

	private DBConnection() {
	}

	/**
	 * Gets the connection object of the database if it is null then returns the
	 * current connection object.
	 * <p>
	 * In case the connection object already exists just returns the connection
	 * object.
	 * 
	 * @return connection object of the database.
	 * @throws DBException
	 * @see Connection
	 */
	public static Connection getConnection() throws DBException {
		if (conn == null) {
			try {
				Class.forName(driver);
				conn = DriverManager.getConnection(connection, user, password);
			} catch (SQLException | ClassNotFoundException e) {
				throw new DBException("Unable to create connection", e);
			}
		}
		return conn;
	}
}