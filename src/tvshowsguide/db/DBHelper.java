package tvshowsguide.db;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import tvshowsguide.entities.Episode;
import tvshowsguide.entities.ImageSize;
import tvshowsguide.entities.Season;
import tvshowsguide.entities.Show;
import tvshowsguide.entities.User;
import tvshowsguide.exceptions.DBException;
import tvshowsguide.utils.DBUtils;
import tvshowsguide.utils.DateUtils;
import tvshowsguide.utils.UserSession;

/**
 * Queries the database to perform the insert update and delete operations.
 *
 */
public class DBHelper {
	DBUtils dbUtils = new DBUtils();

	/**
	 * Inserts the Show in database, based on the given show object
	 * 
	 * @param show
	 * @throws DBException
	 */
	public void insertShow(Show show) throws DBException {
		String query = "INSERT INTO shows VALUES (?, ?, ?, ?)";
		PreparedStatement ps = dbUtils.getPreparedStatement(query);
		try {
			ps.setInt(1, show.getIds().getTrakt());
			ps.setString(2, show.getTitle());
			ps.setString(3, show.getOverview());
			ps.setString(4, show.getImages().getPoster().getThumb());
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new DBException("Unable to set params for prepare statement.", e);
		}
	}

	/**
	 * Inserts the Season in database, based on the given season object
	 * 
	 * @param season
	 * @throws DBException
	 */
	public void insertSeason(Season season) throws DBException {
		String query = "INSERT INTO seasons VALUES (?, ?, ?, ?, ?)";
		PreparedStatement ps = dbUtils.getPreparedStatement(query);
		try {
			ps.setInt(1, season.getIds().getTrakt());
			ps.setInt(2, season.getShowId());
			ps.setString(3, season.getOverview());
			ps.setInt(4, season.getNumber());
			ps.setString(5, season.getImages().getPoster().getThumb());
			ps.executeUpdate();

		} catch (SQLException e) {
			throw new DBException("Unable to set params for prepare statement.", e);
		}
	}

	/**
	 * Inserts the Episode in database, based on the given episode object
	 * 
	 * @param episode
	 * @throws DBException
	 */
	public void insertEpisode(Episode episode) throws DBException {
		String query = "INSERT INTO episodes VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
		PreparedStatement ps = dbUtils.getPreparedStatement(query);
		try {
			ps.setInt(1, episode.getIds().getTrakt());
			ps.setInt(2, episode.getSeasonId());
			ps.setInt(3, episode.getShowId());
			ps.setInt(4, episode.getNumber());
			ps.setString(5, episode.getTitle());
			ps.setString(6, episode.getOverview());
			ps.setDouble(7, episode.getRating());
			ps.setInt(8, episode.getVotes());
			ps.setTimestamp(9, DateUtils.convertISOToSQL(episode.getUpdated_at()));
			ps.setTimestamp(10, DateUtils.convertISOToSQL(episode.getFirst_aired()));
			ps.setString(11, episode.getImages().getScreenshot().getThumb());
			ps.executeUpdate();

		} catch (SQLException e) {
			throw new DBException("Unable to set params for prepare statement.", e);
		}
	}

	/**
	 * Queries the database to return the episode object by given episode ID.
	 * 
	 * @param id
	 * @return Episode
	 * @throws DBException
	 */
	public Episode getEpisodeById(int id) throws DBException {
		String query = "SELECT * FROM episodes where episode_id = " + id;
		ResultSet rs = dbUtils.executeSelectQuery(query);
		Episode episode = new Episode();
		try {
			if (rs.next()) {
				episode.ids.setTrakt(rs.getInt("episode_id"));
				episode.setSeasonId(rs.getInt("season_id"));
				episode.setShowId(rs.getInt("show_id"));
				episode.setNumber(rs.getInt("episode_number"));
				episode.setTitle(rs.getString("title"));
				episode.setOverview(rs.getString("overview"));
				episode.setRating(rs.getDouble("rating"));
				episode.setVotes(rs.getInt("votes"));
				episode.setUpdated_at(rs.getTimestamp("updated_at").toString());
				if (!(rs.getTimestamp("first_aired") == null)) {
					episode.setFirst_aired(rs.getTimestamp("first_aired").toString());
				}
				ImageSize i = new ImageSize();
				i.setThumb(rs.getString("image_url"));
				episode.images.setScreenshot(i);
			}
		} catch (SQLException e) {
			throw new DBException("Unable to retrieve episode for given Id", e);
		}
		return episode;
	}

	/**
	 * Queries the database to return the season object by given season ID.
	 * 
	 * @param id
	 * @return Season
	 * @throws DBException
	 */
	public Season getSeasonById(int id) throws DBException {
		String query = "SELECT * FROM seasons where season_id = " + id;
		ResultSet rs = dbUtils.executeSelectQuery(query);
		Season season = new Season();
		try {
			if (rs.next()) {
				season.ids.setTrakt(rs.getInt("season_id"));
				season.setShowId(rs.getInt("show_id"));
				season.setNumber(rs.getInt("season_num"));
				season.setOverview(rs.getString("overview"));
				ImageSize i = new ImageSize();
				i.setThumb(rs.getString("image_url"));
				season.images.setPoster(i);
			}
		} catch (SQLException e) {
			throw new DBException("Unable to retrieve episode for given Id", e);
		}
		return season;
	}

	/**
	 * Queries the database to return the show object by given show ID.
	 * 
	 * @param id
	 * @return Show
	 * @throws DBException
	 */
	public Show getShowById(int id) throws DBException {
		String query = "SELECT * FROM shows where show_id = " + id;
		ResultSet rs = dbUtils.executeSelectQuery(query);
		Show show = new Show();
		try {
			if (rs.next()) {
				show.ids.setTrakt(rs.getInt("show_id"));
				show.setTitle(rs.getString("name"));
				show.setOverview(rs.getString("overview"));
				ImageSize i = new ImageSize();
				i.setThumb(rs.getString("image_url"));
				show.images.setPoster(i);
			}
		} catch (SQLException e) {
			throw new DBException("Unable to retrieve episode for given Id", e);
		}
		return show;
	}

	/**
	 * Queries the database to return the list seasons for give by given show
	 * ID.
	 * 
	 * @param Show id
	 * @return List of Seasons
	 * @throws DBException
	 */
	public List<Season> getAllSeasonsByShowId(int id) throws DBException {
		String query = "SELECT s.season_id FROM shows sh JOIN seasons s ON sh.show_id = s.show_id WHERE sh.show_id = " + id
				+ " ORDER BY s.season_num DESC";
		List<Season> seasons = new ArrayList<Season>();
		ResultSet rs = dbUtils.executeSelectQuery(query);
		try {
			while (rs.next()) {
				seasons.add(getSeasonById(rs.getInt("season_id")));
			}
		} catch (SQLException e) {
			throw new DBException("Unable to processes seasons for given show", e);
		}
		return seasons;
	}

	/**
	 * Queries the database to return the list episodes for give by given season
	 * ID.
	 * 
	 * @param Season id
	 * @return List of Episodes
	 * @throws DBException
	 */
	public List<Episode> getAllEpisodesBySeasonId(int id) throws DBException {
		String query = "SELECT e.episode_id,s.season_num FROM seasons s JOIN episodes e ON s.season_id = e.season_id WHERE s.season_id = " + id
				+ " ORDER BY e.episode_number";
		List<Episode> episodes = new ArrayList<Episode>();
		ResultSet rs = dbUtils.executeSelectQuery(query);
		try {
			while (rs.next()) {
				Episode episode = getEpisodeById(rs.getInt("episode_id"));
				episode.setSeason(rs.getInt("season_num"));
				episodes.add(episode);
			}
		} catch (SQLException e) {
			throw new DBException("Unable to processes episodes for given season", e);
		}
		return episodes;
	}

	/**
	 * Inserts user by the user object into the database.
	 * 
	 * @param user
	 * @throws DBException
	 */
	public void addUser(User user) throws DBException {
		String query = "INSERT INTO user VALUES (?, ?, ?, ?)";
		PreparedStatement ps = dbUtils.getPreparedStatement(query);
		try {
			ps.setString(1, user.getUsername());
			ps.setString(2, user.getFirstname());
			ps.setString(3, user.getLastname());
			ps.setString(4, user.getPassword());
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new DBException("Unable to set params for prepare statement.", e);
		}
	}

	/**
	 * Checks if the given user name already exists in the database
	 * 
	 * @param username
	 * @return True if user name already exists in database.
	 *         <p>
	 *         False if user name does not exists in database.
	 * 
	 * @throws DBException
	 */
	public boolean checkUsername(String username) throws DBException {
		String query = "SELECT username FROM user WHERE username = ?";
		PreparedStatement ps = dbUtils.getPreparedStatement(query);
		try {
			ps.setString(1, username);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				return false;
			}
		} catch (SQLException e) {
			throw new DBException("Unable to set params for prepare statement.", e);
		}
		return true;
	}

	/**
	 * Checks if the given user name and password matches the record from the
	 * database.
	 * 
	 * @param User
	 * @return True if user is successfully authenticated.
	 *         <p>
	 *         False if user is not successfully authenticated.
	 * 
	 * @throws DBException
	 */
	public boolean validateUser(User user) throws DBException {
		String query = "SELECT username FROM user WHERE username = ? and password = ?";
		PreparedStatement ps = dbUtils.getPreparedStatement(query);
		try {
			ps.setString(1, user.getUsername());
			ps.setString(2, user.getPassword());
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				return true;
			}
		} catch (SQLException e) {
			throw new DBException("Unable to set params for prepare statement.", e);
		}
		return false;
	}

	/**
	 * Inserts or deletes user subscription for a show based on the given show
	 * id and flag.
	 * 
	 * @param showId
	 * @param flag <ul>
	 *        <li>send true if the show needs to be added to the users
	 *        subscription list.
	 *        <li>send true if the show needs to be deleted from the users
	 *        subscription list.
	 *        </ul>
	 * @throws DBException
	 */
	public void insertDeleteSubscription(int showId, boolean flag) throws DBException {
		String query;
		if (flag) {
			query = "INSERT INTO subscription VALUES (?, ?)";
		} else {
			query = "DELETE FROM subscription WHERE userid = ? and showid = ?";
		}
		PreparedStatement ps = dbUtils.getPreparedStatement(query);
		try {
			ps.setString(1, UserSession.getUserid());
			ps.setInt(2, showId);
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new DBException("Unable to set params for prepare statement.", e);
		}
	}

	/**
	 * Check if the show with given id is already in the users subscription
	 * list.
	 * 
	 * @param showId
	 * @return True if the show is already in the users subscription list.
	 *         <p>
	 *         False if the show is not in the users subscription list.
	 * @throws DBException
	 */
	public boolean isSubscribedShow(int showId) throws DBException {
		String query = "SELECT 1 FROM subscription WHERE userid = ? and showid = ?";
		PreparedStatement ps = dbUtils.getPreparedStatement(query);
		try {
			ps.setString(1, UserSession.getUserid());
			ps.setInt(2, showId);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				return true;
			}
		} catch (SQLException e) {
			throw new DBException("Unable to set params for prepare statement.", e);
		}
		return false;
	}

	/**
	 * Check if the show with given id is in the database
	 * 
	 * @param showId
	 * @return True if the show is already in the database.
	 *         <p>
	 *         False if the show is not in the database.
	 * @throws DBException
	 */
	public boolean isShowExists(int showId) throws DBException {
		String query = "SELECT 1 FROM shows WHERE show_id = ?";
		PreparedStatement ps = dbUtils.getPreparedStatement(query);
		try {
			ps.setInt(1, showId);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				return true;
			}
		} catch (SQLException e) {
			throw new DBException("Unable to set params for prepare statement.", e);
		}
		return false;
	}

	/**
	 * Gets the list of shows subscribed by the user
	 * 
	 * @return List of Shows
	 * @throws DBException
	 */
	public List<Show> getSubscribedShows() throws DBException {
		String query = "SELECT showid FROM subscription WHERE userid = ? ";
		PreparedStatement ps = dbUtils.getPreparedStatement(query);
		List<Show> shows = new ArrayList<Show>();
		try {
			ps.setString(1, UserSession.getUserid());
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				shows.add(getShowById(rs.getInt("showid")));
			}
		} catch (SQLException e) {
			throw new DBException("Unable to processes Subscribed shows for the current user", e);
		}
		return shows;
	}

	/**
	 * Gets the list of upcoming episodes based the number of lookup days.
	 * <p>
	 * Based on the lookup days the function gets all the episodes between the
	 * current date and current date + lookup days
	 * 
	 * @param lookupdays
	 * @return List of Episodes
	 * @throws DBException
	 */
	public List<Episode> getUpcomingEpisodes(int lookupdays) throws DBException {
		String query = "SELECT sh.name,sea.season_num, sh.image_url, e.title,e.episode_id FROM episodes e join subscription s on e.show_id = s.showid "
				+ " join shows sh on sh.show_id = e.show_id join seasons sea on e.season_id = sea.season_id "
				+ "WHERE s.userid=? and e.first_aired BETWEEN curdate() and ADDDATE(curdate(), " + lookupdays + ") " + "ORDER BY e.first_aired";
		return getEpisodesList(query);
	}

	/**
	 * Gets the list of recently aired episodes based the number of lookup days.
	 * <p>
	 * Based on the lookup days the function gets all the episodes between the
	 * current date and current date - lookup days
	 * 
	 * @param lookupdays
	 * @return List of Episodes
	 * @throws DBException
	 */
	public List<Episode> getRecentEpisodes(int lookupdays) throws DBException {
		String query = "SELECT sh.name,sea.season_num, sh.image_url, e.title,e.episode_id "
				+ "FROM episodes e join subscription s on e.show_id = s.showid "
				+ " join shows sh on sh.show_id = e.show_id join seasons sea on e.season_id = sea.season_id "
				+ "WHERE s.userid=? and e.first_aired BETWEEN SUBDATE(curdate(), " + lookupdays + ") AND curdate() " + "ORDER BY e.first_aired DESC";

		return getEpisodesList(query);
	}

	/**
	 * Gets the list of episodes based the given query.
	 * 
	 * @param query
	 * @return List of Episodes
	 * @throws DBException
	 */
	private List<Episode> getEpisodesList(String query) throws DBException {
		List<Episode> episodes = new ArrayList<Episode>();
		PreparedStatement ps = dbUtils.getPreparedStatement(query);
		try {
			ps.setString(1, UserSession.getUserid());
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				Episode episode = getEpisodeById(rs.getInt("episode_id"));
				ImageSize i = new ImageSize();
				i.setThumb(rs.getString("image_url"));
				episode.images.setScreenshot(i);
				episode.setSeason(rs.getInt("season_num"));
				episode.setShowName(rs.getString("name"));
				episodes.add(episode);
			}
		} catch (SQLException e) {
			throw new DBException("Unable to processes Upcoming episodes for the current user", e);
		}
		return episodes;
	}

}
