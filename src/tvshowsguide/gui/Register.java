package tvshowsguide.gui;

import javax.swing.BorderFactory;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JPasswordField;
import javax.swing.border.Border;

import tvshowsguide.db.DBHelper;
import tvshowsguide.entities.User;
import tvshowsguide.exceptions.DBException;
import tvshowsguide.utils.GUIUtils;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;

/**
 * Creates the applications registration frame where user can create his/her
 * account to access the application and set up credentials to authenticate
 * them.
 *
 */
public class Register extends JDialog {

	private static final long serialVersionUID = 4411363043085033945L;
	private DBHelper db = new DBHelper();

	private Font font = new Font("Tahoma", Font.PLAIN, 11);
	private JPanel contentPane;

	private JLabel lblFirstName;
	private JLabel lblLastName;
	private JLabel lblPassword;
	private JLabel lblConfirmPassword;
	private JLabel lblUserName;

	private JTextField txtFirstName;
	private JTextField txtLastName;
	private JTextField txtUserName;

	private JPasswordField password;
	private JPasswordField passwordConfirm;

	private JButton btnSubmit;
	private JButton btnCancel;
	private JLabel lblMsg;

	/**
	 * Create the frame.
	 */
	public Register() {
		initialize();
	}

	/**
	 * Initializes the contents of the frame.
	 */
	private void initialize() {
		setFrameProperties();

		lblUserName = new JLabel("User Name");
		lblUserName.setFont(font);
		lblUserName.setBounds(15, 10, 100, 22);
		contentPane.add(lblUserName);

		txtUserName = new JTextField();
		txtUserName.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				validateUsername();
			}
		});
		txtUserName.setFont(font);
		txtUserName.setBounds(110, 10, 200, 22);
		contentPane.add(txtUserName);

		lblFirstName = new JLabel("First Name");
		lblFirstName.setFont(font);
		lblFirstName.setBounds(15, 40, 100, 22);
		contentPane.add(lblFirstName);

		txtFirstName = new JTextField();
		txtFirstName.setFont(font);
		txtFirstName.setBounds(110, 40, 200, 22);
		contentPane.add(txtFirstName);

		lblLastName = new JLabel("Last Name");
		lblLastName.setFont(font);
		lblLastName.setBounds(15, 70, 100, 22);
		contentPane.add(lblLastName);

		txtLastName = new JTextField();
		txtLastName.setFont(font);
		txtLastName.setBounds(110, 70, 200, 22);
		contentPane.add(txtLastName);

		lblPassword = new JLabel("Password");
		lblPassword.setFont(font);
		lblPassword.setBounds(15, 100, 100, 22);
		contentPane.add(lblPassword);

		password = new JPasswordField();
		password.setFont(font);
		password.setBounds(110, 100, 200, 22);
		contentPane.add(password);

		lblConfirmPassword = new JLabel("Confirm Password");
		lblConfirmPassword.setFont(font);
		lblConfirmPassword.setBounds(15, 130, 100, 22);
		contentPane.add(lblConfirmPassword);

		passwordConfirm = new JPasswordField();
		passwordConfirm.setFont(font);
		passwordConfirm.setBounds(110, 130, 200, 22);
		contentPane.add(passwordConfirm);

		btnSubmit = new JButton("Submit");
		btnSubmit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				clickbtnSubmit();
			}
		});
		btnSubmit.setFont(font);
		btnSubmit.setBounds(110, 160, 85, 22);
		contentPane.add(btnSubmit);

		btnCancel = new JButton("Clear");
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				clickbtnCancel();
			}
		});
		btnCancel.setFont(font);
		btnCancel.setBounds(220, 160, 85, 22);
		contentPane.add(btnCancel);

		lblMsg = new JLabel();
		lblMsg.setBounds(15, 194, 320, 14);
		contentPane.add(lblMsg);
	}

	/**
	 * Checks if the user name already exists in the data source. if the user
	 * name already exists in the data source the user name text field border is
	 * set to red.
	 * 
	 */
	private void validateUsername() {
		try {
			boolean val = db.checkUsername(txtUserName.getText());
			Border border = BorderFactory.createLineBorder(Color.RED, 1);
			Border borderDefault = BorderFactory.createLineBorder(new Color(171, 173, 179), 1);
			if (!val) {
				txtUserName.setBorder(border);
				lblMsg.setText("User name is already taken");
			} else {
				txtUserName.setBorder(borderDefault);
				lblMsg.setText("");
			}
		} catch (DBException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Set the frame properties
	 */
	private void setFrameProperties() {
		setTitle("Register");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 361, 257);
		contentPane = new JPanel();
		contentPane.setLayout(null);
		setContentPane(contentPane);
		setModalityType(DEFAULT_MODALITY_TYPE);
	}

	/**
	 * This is called on submit button click. The required fields in the form
	 * are validated.
	 * 
	 */
	@SuppressWarnings("deprecation")
	private void clickbtnSubmit() {
		if (GUIUtils.requiredFields(txtUserName, txtFirstName, txtLastName, password, passwordConfirm)) {
			if (GUIUtils.compareFields(password, passwordConfirm)) {
				User user = new User();
				user.setFirstname(txtFirstName.getText());
				user.setLastname(txtLastName.getText());
				user.setUsername(txtUserName.getText());
				user.setPassword(password.getText());
				try {
					db.addUser(user);
					this.dispose();
				} catch (DBException e) {
					e.printStackTrace();
				}
			} else {
				lblMsg.setText("Passwords dont match");
			}
		} else {
			lblMsg.setText("All fields are required");
		}
	}

	/**
	 * This method is called on Clear button click and clears all the text
	 * fields in the form.
	 * 
	 */
	private void clickbtnCancel() {
		txtFirstName.setText("");
		txtLastName.setText("");
		txtUserName.setText("");
		password.setText("");
		passwordConfirm.setText("");
	}
}
