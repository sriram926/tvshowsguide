package tvshowsguide.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.SpringLayout;

import tvshowsguide.MediaHelper;
import tvshowsguide.entities.Season;
import tvshowsguide.entities.Show;
import tvshowsguide.exceptions.DBException;
import tvshowsguide.uitheme.DarkScrollBarUI;
import tvshowsguide.uitheme.DarkTabbedPaneUI;
import tvshowsguide.utils.GUIUtils;

/**
 * Creates the applications ShowTabpage with tabs for shows overview tab pane
 * and renders the season tab panes based on the user request.
 *
 */
public class ShowTabPage extends JTabbedPane {

	private static final long serialVersionUID = 1L;
	private Show show;
	private JPanel mainContainer = new JPanel();
	public JButton btn;

	public ShowTabPage() {

	}

	/**
	 * Set the Tab Placement to top and UI to DarkTabbedPaneUI
	 * <p>
	 * Also renders the content for shows overview tab pane with the all the
	 * seasons panels.
	 * 
	 */
	public ShowTabPage(Show show) {
		this.show = show;
		setTabPlacement(JTabbedPane.TOP);
		setUI(new DarkTabbedPaneUI());
		mainContainer.setLayout(new BorderLayout(0, 0));
		renderPoster();
		renderOverviewPanel();
		addTab("Overview", mainContainer);
	}

	/**
	 * Renders the content for shows overview.
	 * 
	 */
	private void renderOverviewPanel() {
		JPanel dataContainer = new JPanel();
		SpringLayout springlayout = new SpringLayout();
		dataContainer.setLayout(springlayout);
		dataContainer.setBackground(Color.BLACK);
		mainContainer.add(dataContainer);

		JLabel title = new JLabel(show.getTitle());
		title.setForeground(Color.WHITE);
		springlayout.putConstraint(SpringLayout.NORTH, title, 10, SpringLayout.NORTH, dataContainer);
		springlayout.putConstraint(SpringLayout.WEST, title, 10, SpringLayout.WEST, dataContainer);
		springlayout.putConstraint(SpringLayout.EAST, title, -20, SpringLayout.EAST, dataContainer);
		dataContainer.add(title);

		JTextArea textarea = new JTextArea(show.getOverview(), 50, 100);
		textarea.setForeground(Color.LIGHT_GRAY);
		springlayout.putConstraint(SpringLayout.NORTH, textarea, 20, SpringLayout.SOUTH, title);
		springlayout.putConstraint(SpringLayout.WEST, textarea, 10, SpringLayout.WEST, dataContainer);
		springlayout.putConstraint(SpringLayout.SOUTH, textarea, 150, SpringLayout.SOUTH, title);
		springlayout.putConstraint(SpringLayout.EAST, textarea, -20, SpringLayout.EAST, dataContainer);
		dataContainer.add(textarea);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		scrollPane.getVerticalScrollBar().setUnitIncrement(16);
		scrollPane.getVerticalScrollBar().setValue(0);

		scrollPane.setBackground(new Color(0, 0, 0));
		scrollPane.getViewport().setBackground(Color.BLACK);
		scrollPane.setOpaque(false);
		scrollPane.getVerticalScrollBar().setUI(new DarkScrollBarUI());

		JPanel seasons = getSeasonsPanel();
		scrollPane.getViewport().add(seasons);
		scrollPane.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
		springlayout.putConstraint(SpringLayout.NORTH, scrollPane, 10, SpringLayout.SOUTH, textarea);
		springlayout.putConstraint(SpringLayout.WEST, scrollPane, 3, SpringLayout.WEST, dataContainer);
		springlayout.putConstraint(SpringLayout.EAST, scrollPane, -1, SpringLayout.EAST, dataContainer);
		springlayout.putConstraint(SpringLayout.SOUTH, scrollPane, 0, SpringLayout.SOUTH, dataContainer);
		dataContainer.add(scrollPane);
		scrollPane.setPreferredSize(dataContainer.getSize());
		textarea.setOpaque(false);
		textarea.setLineWrap(true);
		textarea.setWrapStyleWord(true);
		textarea.setEditable(false);
	}

	/**
	 * Renders the panel with all the season for the show.
	 * 
	 * @return JPanel
	 */
	private JPanel getSeasonsPanel() {
		JPanel listContainer = new JPanel();
		listContainer.setLayout(new BoxLayout(listContainer, BoxLayout.Y_AXIS));
		try {
			List<Season> seasons = new MediaHelper().getAllSeasonsByShowId(show.ids.getTrakt());
			for (Season season : seasons) {
				listContainer.add(new SeasonPanel(season).getPanel());
			}
		} catch (DBException e) {
			e.printStackTrace();
		}
		return listContainer;
	}

	/**
	 * Renders the show poster to the left top corner of the overview tab pane.
	 * 
	 */
	private void renderPoster() {
		JPanel posterPanel = new JPanel();
		posterPanel.setBorder(BorderFactory.createEmptyBorder(0, 25, 0, 0));
		JLabel poster = GUIUtils.getImageFromURL(show.images.getPoster().getThumb(), 300, 450);
		posterPanel.add(poster, BorderLayout.NORTH);
		posterPanel.setBackground(Color.BLACK);
		mainContainer.add(posterPanel, BorderLayout.WEST);
	}
}
