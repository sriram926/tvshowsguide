package tvshowsguide.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.border.Border;

import tvshowsguide.Global;
import tvshowsguide.entities.Episode;
import tvshowsguide.exceptions.DBException;
import tvshowsguide.utils.DateUtils;
import tvshowsguide.utils.GUIUtils;

/**
 * Generates the Episode JPanel based on the given episode object.
 *
 */
public class EpisodePanel extends JPanel {

	private static final long serialVersionUID = 1L;
	private Episode episode;
	private Color border = Global.getInstance().uicolor_primary;
	private Color background = Global.getInstance().uicolor_secondary;
	private Color foreground;
	private int useScreenShot = 0;

	/**
	 * Set the episode object in the EpisodeSummaryPanel
	 * 
	 * @param episode
	 */
	public EpisodePanel(Episode episode) {
		this.episode = episode;
		this.foreground = Color.WHITE;
	}

	/**
	 * Set the episode object in the EpisodeSummaryPanel and use the episode
	 * screenshot in case argument useScreenShot 1 else uses the poster of it
	 * show.
	 * 
	 * @param episode
	 */
	public EpisodePanel(Episode episode, int useScreenShot) {
		this.useScreenShot = useScreenShot;
		this.episode = episode;
		this.foreground = Color.WHITE;
	}

	/**
	 * Gets the Panel with Episode Summary
	 * 
	 * @return JPanel with Episode Summary as content
	 * @throws DBException
	 */
	public JPanel getPanel() throws DBException {
		JPanel mainContainer = new JPanel();
		mainContainer.setBackground(background);
		mainContainer.setLayout(new FlowLayout());

		Border paneEdge = BorderFactory.createLineBorder(border, 10);
		mainContainer.setBorder(paneEdge);

		JPanel picContainer = new JPanel();
		JPanel dataContainer = new JPanel();
		dataContainer.setBackground(background);
		picContainer.setBackground(background);
		addData(dataContainer);
		if (useScreenShot == 0) {
			picContainer.add(GUIUtils.getImageFromURL(episode.images.getScreenshot().getThumb()));
		} else {
			picContainer.add(GUIUtils.getImageFromURL(episode.images.getScreenshot().getThumb(), 250, 200));
		}
		mainContainer.add(picContainer);
		mainContainer.add(dataContainer);
		return mainContainer;
	}

	/**
	 * Add episodes information to the dataContainer of the Episode Panel
	 * 	
	 */
	private void addData(JPanel dataContainer) {
		JPanel titlepanel = new JPanel();
		JLabel title = new JLabel("S" + episode.getSeason() + ", Ep" + episode.getNumber() + " - " + episode.getTitle());
		title.setForeground(foreground);
		titlepanel.setBackground(background);
		titlepanel.add(title);

		JTextArea ta = new JTextArea(episode.getOverview(), 6, 35);
		ta.setOpaque(false);
		ta.setLineWrap(true);
		ta.setWrapStyleWord(true);
		ta.setEditable(false);
		ta.setForeground(foreground);

		JPanel airDatesContatiner = new JPanel();
		airDatesContatiner.setBackground(background);
		JLabel airDate = new JLabel(DateUtils.formatDate(episode.getFirst_aired()));
		airDate.setForeground(foreground);
		airDatesContatiner.add(new JPanel().add(GUIUtils.getImageFromPath(Global.ICON_CLOCK, 15, 15)), BorderLayout.WEST);
		airDatesContatiner.add(airDate);

		JPanel overviewPanel = new JPanel();
		overviewPanel.setBackground(background);
		overviewPanel.setLayout(new BoxLayout(overviewPanel, BoxLayout.Y_AXIS));
		overviewPanel.add(ta);
		overviewPanel.add(airDatesContatiner);

		dataContainer.setLayout(new BorderLayout());
		dataContainer.add(titlepanel, BorderLayout.NORTH);
		dataContainer.add(overviewPanel, BorderLayout.CENTER);
	}
}
