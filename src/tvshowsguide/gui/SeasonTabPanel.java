package tvshowsguide.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.border.Border;

import tvshowsguide.Global;
import tvshowsguide.MediaHelper;
import tvshowsguide.entities.Episode;
import tvshowsguide.entities.Season;
import tvshowsguide.exceptions.DBException;
import tvshowsguide.uitheme.AlternateDarkScrollBarUI;
import tvshowsguide.utils.GUIUtils;

/**
 * Generates the season summary JPanel based on the given season object and adds
 * the panel to the show tab page as a new closable tab pane if the season is
 * not already added. If the season tab pane is already added to the show tab
 * page, it is set to be selected.
 *
 */
public class SeasonTabPanel {

	private JTabbedPane showTabPanel;
	private Season season;
	private MediaHelper mediahelper = new MediaHelper();

	/**
	 * Renders the season summary JPanel based on the given season object and
	 * adds the panel to the show tab page as a new closable tab pane if the
	 * season is not already added. If the season tab pane is already added to
	 * the show tab page, it is set to be selected.
	 * 
	 * @param showTabPanel
	 * @param season
	 */
	public SeasonTabPanel(final JTabbedPane showTabPanel, Season season) {
		this.showTabPanel = showTabPanel;
		this.season = season;
		if (checkIfPanelOpened()) {
			return;
		}
		try {
			JScrollPane scrollPane = new JScrollPane();
			scrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
			scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
			scrollPane.getVerticalScrollBar().setUnitIncrement(16);
			scrollPane.getVerticalScrollBar().setValue(0);
			scrollPane.getVerticalScrollBar().setUI(new AlternateDarkScrollBarUI());
			JPanel episodes = getEpisodesList();
			scrollPane.getViewport().add(episodes);
			showTabPanel.addTab(null, scrollPane);
			renderTabHeader();
		} catch (DBException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Checks if the season tab pane is already added to the show tab page.
	 * 
	 * @return true if the season tab pane is already added to the show tab.
	 *         page.
	 *         <p>
	 *         false if season tab pane is not added to the show tab page.
	 * 
	 */
	private boolean checkIfPanelOpened() {
		for (int i = 1; i < showTabPanel.getTabCount(); i++) {
			JPanel pnl = (JPanel) showTabPanel.getTabComponentAt(i);
			JLabel lbl = (JLabel) pnl.getComponent(0);
			String btnCmd = lbl.getText();
			if (btnCmd.equals(season.getName())) {
				showTabPanel.setSelectedIndex(i);
				return true;
			}
		}
		return false;
	}

	/**
	 * Renders the tab header with the season number and add close button to
	 * remove the tab pane.
	 */
	private void renderTabHeader() {
		int currentTabIndex = showTabPanel.getTabCount() - 1;
		JPanel tabheader = new JPanel();

		FontMetrics metrics = showTabPanel.getFontMetrics(new Font("Verdana", 1, 17));
		int width = metrics.stringWidth(season.getName());
		tabheader.setPreferredSize(new Dimension(width, 18));
		tabheader.setLayout(new BorderLayout(0, 0));
		tabheader.setOpaque(false);

		JLabel title = new JLabel(season.getName());
		title.setForeground(Color.WHITE);
		title.setBorder(BorderFactory.createEmptyBorder(0, 2, 0, 0));
		JButton tabCloseButton = new JButton();
		tabCloseButton.setOpaque(false);
		tabCloseButton.setContentAreaFilled(false);
		tabCloseButton.setBorderPainted(false);
		tabCloseButton.setFocusPainted(false);
		tabCloseButton.setActionCommand("" + currentTabIndex);

		Border paneEdge = BorderFactory.createLineBorder(Color.black, 2);
		tabCloseButton.setBorder(paneEdge);
		tabCloseButton.setIcon(GUIUtils.getImageIconFromPath(Global.ICON_CLOSE, 17, 17));
		tabheader.add(title, BorderLayout.WEST);
		tabheader.add(tabCloseButton, BorderLayout.EAST);

		tabCloseButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JButton btn = (JButton) e.getSource();
				String currentBtnCmd = btn.getActionCommand();
				for (int i = 1; i < showTabPanel.getTabCount(); i++) {
					JPanel pnl = (JPanel) showTabPanel.getTabComponentAt(i);
					btn = (JButton) pnl.getComponent(1);
					String btnCmd = btn.getActionCommand();
					if (currentBtnCmd.equals(btnCmd)) {
						showTabPanel.removeTabAt(i);
						break;
					}
				}
			}
		});
		showTabPanel.setTabComponentAt(currentTabIndex, tabheader);
		showTabPanel.setSelectedIndex(currentTabIndex);
	}

	/**
	 * Adds the episode panels for the episodes in the given season to a JPanel.
	 * 
	 * @return JPanel
	 * @throws DBException
	 */
	private JPanel getEpisodesList() throws DBException {
		List<Episode> episodes = mediahelper.getAllEpisodesBySeasonId(season.ids.getTrakt());
		JPanel listContainer = new JPanel();
		listContainer.setLayout(new BoxLayout(listContainer, BoxLayout.Y_AXIS));
		for (Episode episode : episodes) {
			listContainer.add(new EpisodePanel(episode, 1).getPanel());
		}
		return listContainer;
	}
}
