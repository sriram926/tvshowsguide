package tvshowsguide.gui;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;

import tvshowsguide.Main;
import tvshowsguide.db.DBHelper;
import tvshowsguide.entities.User;
import tvshowsguide.exceptions.DBException;
import tvshowsguide.utils.GUIUtils;
import tvshowsguide.utils.UserSession;

/**
 * Creates the applications Login frame where user can enter his/her credentials
 * to authenticate them or can open register frame if he/her is a new user.
 *
 */
public class Login extends JFrame {

	private DBHelper db = new DBHelper();
	private static final long serialVersionUID = 6217303794896215941L;
	private Font font = new Font("Tahoma", Font.PLAIN, 11);
	private JPanel contentPane;
	private JLabel lblUserName;
	private JTextField txtUserName;
	private JLabel lblPassword;
	private JPasswordField passwordField;
	private JLabel lblMsg;

	/**
	 * Create the frame.
	 */
	public Login() {
		initialize();

		lblUserName = new JLabel("User name");
		lblUserName.setBounds(27, 28, 90, 14);
		lblUserName.setFont(font);
		contentPane.add(lblUserName);

		txtUserName = new JTextField("sri");
		txtUserName.setBounds(123, 25, 213, 20);
		txtUserName.setFont(font);
		contentPane.add(txtUserName);

		lblPassword = new JLabel("Password");
		lblPassword.setBounds(27, 59, 90, 14);
		lblPassword.setFont(font);
		contentPane.add(lblPassword);

		passwordField = new JPasswordField("123");
		passwordField.setBounds(123, 56, 213, 20);
		passwordField.setFont(font);
		contentPane.add(passwordField);

		lblMsg = new JLabel();
		lblMsg.setBounds(51, 121, 320, 14);
		contentPane.add(lblMsg);

		JButton btnLogin = new JButton("Login");
		btnLogin.setBounds(123, 87, 89, 23);
		btnLogin.setFont(font);
		btnLogin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnLoginClick();
			}
		});
		btnLogin.setBackground(UIManager.getColor("Button.background"));
		contentPane.add(btnLogin);

		JButton btnRegister = new JButton("Register");
		btnRegister.setBounds(247, 87, 89, 23);
		btnRegister.setFont(font);
		btnRegister.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnRegisterClick();
			}
		});
		btnRegister.setBackground(UIManager.getColor("Button.background"));
		contentPane.add(btnRegister);
	}

	/**
	 * Initializes the contents of the frame.
	 */
	private void initialize() {
		setFrameProperties();
	}

	/**
	 * Sets the frame properties
	 */
	private void setFrameProperties() {
		setTitle("Login");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 397, 180);
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		setLocation(dim.width / 2 - this.getSize().width / 2, dim.height / 2 - this.getSize().height / 2);

		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
	}

	/**
	 * This method is called on Register Button click
	 */
	public void btnRegisterClick() {
		Register frameRegister = new Register();
		frameRegister.setVisible(true);
	}

	/**
	 * This method is called on Login Button click
	 */
	@SuppressWarnings("deprecation")
	public void btnLoginClick() {
		if (GUIUtils.requiredFields(txtUserName, passwordField)) {
			try {
				if (!db.validateUser(new User(txtUserName.getText(), passwordField.getText()))) {
					lblMsg.setText("Invalid user name or password");
				} else {
					UserSession.createSession(txtUserName.getText());
					setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
					Main main = new Main();
					main.renderMainFrame();
					dispose();
				}
			} catch (DBException e) {
				e.printStackTrace();
			}
		} else {
			lblMsg.setText("All fields are required");
		}
	}
}
