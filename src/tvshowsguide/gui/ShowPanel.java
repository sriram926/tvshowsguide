package tvshowsguide.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.border.Border;

import tvshowsguide.Global;
import tvshowsguide.MediaHelper;
import tvshowsguide.entities.Show;
import tvshowsguide.enums.TabType;
import tvshowsguide.exceptions.DBException;
import tvshowsguide.utils.GUIUtils;


/**
 * Generates the Shows Summary JPanel based on the given show object.
 *
 */
public class ShowPanel extends JPanel implements ActionListener {
	private static final long serialVersionUID = 1L;

	private Show show;
	private static final String SUBSCRIBE = "SUBSCRIBE";
	private static final String UNSUBSCRIBE = "UNSUBSCRIBE";
	private static final String SHOW_DETAILS = "DETAILS";
	private Color border = Global.getInstance().uicolor_primary;
	private Color background = Global.getInstance().uicolor_secondary;
	private Color foreground;

	private TabType tabtype;
	MediaHelper media = new MediaHelper();

	/**
	 * Set the show object in the ShowsSummaryPanel and the tab for which the
	 * show panel is being rendered.
	 * 
	 * @param show
	 * @param tabtype
	 */
	public ShowPanel(Show show, TabType tabtype) {
		this.show = show;
		this.tabtype = tabtype;
		this.foreground = Color.WHITE;
	}

	/**
	 * Gets the Panel with Show Summary
	 * 
	 * @return JPanel
	 * @throws DBException
	 */
	public JPanel getShowPanel() throws DBException {
		JPanel mainContainer = new JPanel();
		mainContainer.setBackground(background);
		mainContainer.setLayout(new FlowLayout());
		Border paneEdge = BorderFactory.createLineBorder(border, 10);
		mainContainer.setBorder(paneEdge);

		JPanel picContainer = new JPanel();
		JPanel dataContainer = new JPanel();
		JPanel buttonContainer = new JPanel();
		dataContainer.setBackground(background);
		picContainer.setBackground(background);
		buttonContainer.setBackground(background);
		addData(dataContainer);
		picContainer.add(GUIUtils.getImageFromURL(show.images.getPoster().getThumb()));

		JButton subscriptionBtn = Global.createButton();
		subscriptionBtn.addActionListener(this);
		buttonContainer.setLayout(new BorderLayout());
		buttonContainer.add(subscriptionBtn, BorderLayout.NORTH);

		if (!(tabtype == TabType.TRENDING || tabtype == TabType.SEARCH)) {
			JButton showDetailsBtn = Global.createButton();
			showDetailsBtn.setIcon(GUIUtils.getImageIconFromPath(Global.ICON_DETAILS, 40, 40));
			showDetailsBtn.setActionCommand(SHOW_DETAILS);
			showDetailsBtn.addActionListener(this);
			buttonContainer.add(showDetailsBtn, BorderLayout.SOUTH);
		}
		setSubscriptionImage(subscriptionBtn);

		mainContainer.add(picContainer);
		mainContainer.add(dataContainer);
		mainContainer.add(buttonContainer);
		return mainContainer;
	}

	/**
	 * Set the properties of the button to subscribe or unsubscribe the show
	 * based on if the user is already subscribed.
	 * 
	 * @param btn
	 * @throws DBException
	 */
	private void setSubscriptionImage(JButton btn) throws DBException {
		toggleImage(btn, media.checkSubscription(show.ids.getTrakt()));
	}

	/**
	 * Toggles button icon and action command for the show.
	 * <p>
	 * If the user is already subscribed to the show the unsubscribe icon and
	 * action command are set to the button.
	 * <p>
	 * If the user is not subscribed to the show the subscribe icon and action
	 * command are set to the button.
	 * 
	 * @param btn
	 * @throws DBException
	 */
	private void toggleImage(JButton btn, boolean flag) {
		if (flag) {
			btn.setIcon(GUIUtils.getImageIconFromPath(Global.ICON_REMOVE, 40, 40));
			btn.setActionCommand(UNSUBSCRIBE);
		} else {
			btn.setIcon(GUIUtils.getImageIconFromPath(Global.ICON_ADD, 40, 40));
			btn.setActionCommand(SUBSCRIBE);
		}
	}

	/**
	 * Add show information to the dataContainer of the show Panel
	 * 
	 */
	private void addData(JPanel dataContainer) {
		JPanel titlepanel = new JPanel();
		JLabel title = new JLabel(show.getTitle());
		title.setForeground(foreground);
		titlepanel.add(title);
		titlepanel.setBackground(background);
		JTextArea ta = new JTextArea(show.getOverview(), 6, 35);
		ta.setOpaque(false);
		ta.setLineWrap(true);
		ta.setWrapStyleWord(true);
		ta.setEditable(false);
		ta.setForeground(foreground);
		JPanel overviewPanel = new JPanel();
		overviewPanel.add(ta);
		overviewPanel.setBackground(background);
		dataContainer.setLayout(new BorderLayout());
		dataContainer.add(titlepanel, BorderLayout.NORTH);
		dataContainer.add(overviewPanel, BorderLayout.CENTER);
	}

	/**
	 * This method is called on the subscribe/unsubscribe button click and also
	 * on the show details button click.
	 * <p>
	 * Based on the buttons action command, if the action command is set to
	 * subscribe the show is added to the user subscription list and changes the
	 * icon and action command of the button to unsubscribe.
	 * <p>
	 * If the action command is set to unsubscribe the show is removed from the
	 * user subscription list and changes the icon and action command of the
	 * button to subscribe.
	 * <p>
	 * If the action command is set to show details the main frame is set to the
	 * show tab page instead of the home tab page and add the navigation to
	 * return to the home tab page.
	 * 
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		JButton btn = (JButton) e.getSource();
		try {
			if (btn.getActionCommand().equalsIgnoreCase(SUBSCRIBE)) {
				media.addSubscription(show);
				toggleImage(btn, true);
				Global.setSubscriptionstate(1);
			} else if (btn.getActionCommand().equalsIgnoreCase(UNSUBSCRIBE)) {
				media.removeSubscription(show.ids.getTrakt());
				toggleImage(btn, false);
				if (tabtype == TabType.SUBSCRIBED) {
					JPanel c = (JPanel) btn.getParent().getParent().getParent();
					c.remove(btn.getParent().getParent());
					c.updateUI();
				}
				Global.setSubscriptionstate(1);
			} else if (btn.getActionCommand().equalsIgnoreCase(SHOW_DETAILS)) {
				ShowTabPage showTabPanel = new ShowTabPage(show);
				Global.getMainFrame().toggleContainers(showTabPanel);
			}
		} catch (DBException dbe) {
			dbe.printStackTrace();
		}
	}

}
