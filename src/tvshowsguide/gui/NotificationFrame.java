package tvshowsguide.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.WindowConstants;

import tvshowsguide.Global;
import tvshowsguide.entities.Episode;
import tvshowsguide.utils.DateUtils;
import tvshowsguide.utils.GUIUtils;

/**
 * Creates the notification frame for a given episode.
 * <p>
 * This notification frame can be made visible or set to be made visible before
 * certain hours before the episode is aired.
 * <p>
 * The notification frame is set to the location at the bottom right corner of
 * the screen and set above the task bar.
 * <P>
 * The notification frame is automatically disposed after 5 seconds from the
 * time it is displayed.
 * 
 */
public class NotificationFrame extends JDialog {

	private static final long serialVersionUID = 1L;
	private Episode episode;
	private Container contentPane = getContentPane();
	private final long closeafter = 5000;

	/**
	 * Renders the Notification frame based on the given episode.
	 * 
	 * @param episode
	 */
	public NotificationFrame(Episode episode) {
		setFrameProperties();
		this.episode = episode;
		JPanel mainContainer = new JPanel();
		mainContainer.setBackground(Global.getInstance().uicolor_primary);
		mainContainer.setLayout(new FlowLayout());
		mainContainer.setBorder(BorderFactory.createEmptyBorder(0, 2, 2, 0));

		JPanel picContainer = new JPanel();
		JPanel dataContainer = new JPanel();
		dataContainer.setBackground(Global.getInstance().uicolor_primary);
		picContainer.setBackground(Global.getInstance().uicolor_primary);
		addData(dataContainer);
		picContainer.add(GUIUtils.getImageFromURL(episode.images.getScreenshot().getThumb(), 75, 95));

		mainContainer.add(picContainer);
		mainContainer.add(dataContainer);

		contentPane.add(getHeaderPanel(), BorderLayout.NORTH);
		contentPane.add(mainContainer, BorderLayout.CENTER);
	}

	/**
	 * Sets the Frame properties like size, decoration, always on top, location
	 * to the bottom of the screen, background color and Close operation.
	 */
	private void setFrameProperties() {
		setSize(300, 140);
		setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		setUndecorated(true);
		setAlwaysOnTop(true);
		Dimension scrSize = Toolkit.getDefaultToolkit().getScreenSize();
		Insets toolHeight = Toolkit.getDefaultToolkit().getScreenInsets(getGraphicsConfiguration());
		setLocation(scrSize.width - getWidth(), scrSize.height - toolHeight.bottom - getHeight());
	}

	/**
	 * Renders the header panel with close button and notification title.
	 * 
	 * @return JPanel
	 */
	private JPanel getHeaderPanel() {
		JPanel headPanel = new JPanel();
		headPanel.setBackground(Global.getInstance().uicolor_primary);
		headPanel.setLayout(new BorderLayout(0, 0));
		headPanel.setBorder(BorderFactory.createEmptyBorder(0, 5, 0, 0));
		JLabel headingLabel = new JLabel(episode.getShowName() + " - S" + episode.getSeason() + ", Ep" + episode.getNumber());
		headingLabel.setForeground(Color.WHITE);
		JButton btnClose = new JButton();
		btnClose.setBorder(BorderFactory.createEmptyBorder(0, 5, 0, 0));
		btnClose.setOpaque(false);
		btnClose.setContentAreaFilled(false);
		btnClose.setBorderPainted(false);
		btnClose.setFocusPainted(false);
		btnClose.setIcon(GUIUtils.getImageIconFromPath(Global.ICON_REMOVE, 30, 30));
		headPanel.add(headingLabel, BorderLayout.CENTER);
		headPanel.add(btnClose, BorderLayout.EAST);
		btnClose.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		return headPanel;
	}

	/**
	 * Add the episode data to the data container of the notification frame.
	 * 
	 * @param dataContainer
	 */
	private void addData(JPanel dataContainer) {
		JPanel titlepanel = new JPanel();
		JLabel title = new JLabel(episode.getTitle());
		title.setForeground(Color.WHITE);
		titlepanel.setBackground(Global.getInstance().uicolor_primary);
		titlepanel.add(title);

		JPanel airDatesContatiner = new JPanel();
		airDatesContatiner.setBackground(Global.getInstance().uicolor_primary);
		JLabel airDate = new JLabel(DateUtils.formatDate(episode.getFirst_aired()));
		airDate.setForeground(Color.WHITE);
		airDatesContatiner.add(new JPanel().add(GUIUtils.getImageFromPath(Global.ICON_CLOCK, 15, 15)), BorderLayout.WEST);
		airDatesContatiner.add(airDate);
		dataContainer.setLayout(new BorderLayout());
		dataContainer.add(titlepanel, BorderLayout.CENTER);
		dataContainer.add(airDatesContatiner, BorderLayout.SOUTH);
	}

	/**
	 * Displays the notification frame and disposes the frame after 5 seconds.
	 * 
	 */
	public void showNotificationNow() {
		setVisible(true);
		startClosingThread();
	}

	/**
	 * Displays the notification frame before given hours before the episode is
	 * aired and disposes the frame after 5 seconds.
	 * 
	 * @param hoursbefore
	 */
	public void showNotification(int hoursbefore) {
		setTimer(hoursbefore);
	}

	/**
	 * Schedules the timer to show the notification frame before given hours
	 * before the episode is aired.
	 * 
	 * @param hoursbefore
	 */
	private void setTimer(int hoursbefore) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:SS");
		try {
			Date timeAt = sdf.parse(episode.getFirst_aired());
			if (timeAt.before(new Date())) {
				return;
			}
			Calendar cal = Calendar.getInstance();
			cal.setTime(timeAt);
			cal.add(Calendar.HOUR, -hoursbefore);
			Date alertTime = cal.getTime();
			Timer timer = new Timer();
			timer.schedule(new TimerTask() {
				@Override
				public void run() {
					setVisible(true);
					startClosingThread();
				}
			}, alertTime);
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Disposes the frame after 5 seconds.
	 */
	private void startClosingThread() {
		if (closeafter > 0) {
			new Thread() {
				@Override
				public void run() {
					try {
						Thread.sleep(closeafter);
						dispose();
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				};
			}.start();
		}
	}
}
