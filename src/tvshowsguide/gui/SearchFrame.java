package tvshowsguide.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;

import tvshowsguide.Global;
import tvshowsguide.MediaHelper;
import tvshowsguide.entities.Show;
import tvshowsguide.enums.TabType;
import tvshowsguide.exceptions.DBException;
import tvshowsguide.uitheme.AlternateDarkScrollBarUI;
import tvshowsguide.utils.GUIUtils;

/**
 * Creates the applications search frame where user can search for shows with
 * giving string. The application get the search results from the API based on
 * the given string and searches titles, descriptions, translated titles,
 * aliases, and people.
 *
 */
public class SearchFrame extends JDialog {

	private Container contentPane = getContentPane();
	private JPanel mainContainer = new JPanel();
	private static final long serialVersionUID = 1L;
	private MediaHelper mediahelper = new MediaHelper();
	private Color background = Global.getInstance().uicolor_primary;
	private JPanel headPanel = new JPanel();
	private JTextField txtSearch = new JTextField();
	private JScrollPane scrollPane = new JScrollPane();

	/**
	 * Renders search frame with empty results.
	 */
	public SearchFrame() {
		setTitle("TV Guide - Search");
		setModalityType(DEFAULT_MODALITY_TYPE);
		setFrameProperties();
		mainContainer.setBackground(background);
		mainContainer.setLayout(new BoxLayout(mainContainer, BoxLayout.Y_AXIS));
		renderHeaderPanel();
		renderSearchResultsArea();
		contentPane.add(headPanel, BorderLayout.NORTH);
		contentPane.add(mainContainer, BorderLayout.CENTER);
		addWindowListener(new java.awt.event.WindowAdapter() {
			@Override
			public void windowClosing(java.awt.event.WindowEvent windowEvent) {
				try {
					if (Global.getSubscriptionstate() == 1) {
						Global.getMainFrame().refreshSubscribedTab();
					}
				} catch (DBException e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Sets the Frame properties like size, decoration, always on top, location
	 * to the bottom of the screen, background color and Close operation.
	 * 
	 */
	private void setFrameProperties() {
		setBackground(background);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 1080, 680);
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		setLocation(dim.width / 2 - this.getSize().width / 2, dim.height / 2 - this.getSize().height / 2);
	}

	/**
	 * Renders search frame results area with empty results.
	 * 
	 */
	private void renderSearchResultsArea() {
		scrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		scrollPane.getVerticalScrollBar().setUnitIncrement(16);
		scrollPane.getVerticalScrollBar().setValue(0);
		scrollPane.getVerticalScrollBar().setUI(new AlternateDarkScrollBarUI());
		scrollPane.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
		scrollPane.getViewport().setBackground(background);
		scrollPane.setBackground(background);
		mainContainer.add(scrollPane);
	}

	/**
	 * Renders search frame header panel with text field for enter the search
	 * text and search button.
	 * 
	 */
	private void renderHeaderPanel() {
		headPanel.setBackground(background);
		headPanel.setLayout(new BorderLayout(0, 0));
		headPanel.setBorder(BorderFactory.createEmptyBorder(5, 10, 0, 0));
		txtSearch.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));

		JButton btnSearch = Global.createButton();
		btnSearch.setBorder(BorderFactory.createEmptyBorder(5, 10, 0, 10));
		btnSearch.setIcon(GUIUtils.getImageIconFromPath(Global.ICON_SEARCH, 25, 25));

		headPanel.add(txtSearch, BorderLayout.CENTER);
		headPanel.add(btnSearch, BorderLayout.EAST);

		btnSearch.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					getSearchResults();
					mainContainer.updateUI();
				} catch (DBException e1) {
					e1.printStackTrace();
				}
			}
		});
	}

	/**
	 * This method is called on the search button click and get the results from
	 * the data source and renders then on the search results panel.
	 * 
	 * @throws DBException
	 */
	private void getSearchResults() throws DBException {
		if (!txtSearch.getText().contentEquals("")) {
			List<Show> shows = mediahelper.searchForShow(txtSearch.getText());
			JPanel listpanel = new JPanel();
			listpanel.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
			listpanel.setLayout(new BoxLayout(listpanel, BoxLayout.Y_AXIS));
			for (Show show : shows) {
				listpanel.add(new ShowPanel(show, TabType.SEARCH).getShowPanel());
			}
			scrollPane.getViewport().add(listpanel);
		}
	}
}
