package tvshowsguide.gui;

import java.util.List;

import javax.swing.BoxLayout;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import tvshowsguide.Global;
import tvshowsguide.MediaHelper;
import tvshowsguide.entities.Episode;
import tvshowsguide.entities.Show;
import tvshowsguide.enums.TabType;
import tvshowsguide.exceptions.DBException;
import tvshowsguide.uitheme.AlternateDarkScrollBarUI;
import tvshowsguide.uitheme.DarkTabbedPaneUI;

/**
 * Creates the applications HomeTabePage with tabs for subscribed shows,
 * upcoming episodes, recently aired episodes and trending shows.
 *
 */
public class HomeTabPage extends JTabbedPane {

	private static final long serialVersionUID = 1L;
	private JScrollPane scrollTrending = getScrollPane();
	private JScrollPane scrollSuscribed = getScrollPane();
	private JScrollPane scrollUpcoming = getScrollPane();
	private JScrollPane scrollRecent = getScrollPane();
	private MediaHelper mediahelper = new MediaHelper();

	/**
	 * Set the Tab Placement to top and UI to DarkTabbedPaneUI
	 * <p>
	 * Also renders the content for subscribed , upcoming, recent and trending
	 * tab panes.
	 */
	public HomeTabPage() {
		setTabPlacement(JTabbedPane.TOP);
		setUI(new DarkTabbedPaneUI());
		try {
			renderTabPanes();
		} catch (DBException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Renders the content for subscribed , upcoming, recent and trending tab
	 * panes.
	 * 
	 * @throws DBException
	 */
	public void renderTabPanes() throws DBException {
		scrollSuscribed.getViewport().add(getSubscribedShows());
		addTab("Suscribed", scrollSuscribed);
		addTab("Upcoming", scrollUpcoming);
		addTab("Recent", scrollRecent);
		addTab("Trending Now", scrollTrending);
		addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				tabStateChanged(e);
			}
		});
	}

	/**
	 * For every tab state changed based on the Subscription state in the Global
	 * class. If the Subscription state is 1 the current tab pane is reloaded.
	 * 
	 */
	private void tabStateChanged(ChangeEvent e) {
		if (e.getSource() instanceof JTabbedPane) {
			JTabbedPane pane = (JTabbedPane) e.getSource();
			try {
				switch (pane.getSelectedIndex()) {
				case 0:
					if (Global.getSubscriptionstate() == 1) {
						scrollSuscribed.getViewport().add(getSubscribedShows());
						Global.setSubscriptionstate(0);
					}
					break;
				case 1:
					scrollUpcoming.getViewport().add(getUpcomingEpisodes());
					break;

				case 2:
					scrollRecent.getViewport().add(getRecentEpisodes());
					break;
				case 3:
					if (Global.getSubscriptionstate() == 1 || Global.getSubscriptionstate() == 99) {
						scrollTrending.getViewport().add(getTrendingShows());
						Global.setSubscriptionstate(0);
					}
					break;
				}
			} catch (DBException e1) {
				e1.printStackTrace();
			}
		}
	}

	/**
	 * Gets JScrollPane object with properties
	 * <p>
	 * HorizontalScrollBarPolicy set to HORIZONTAL_SCROLLBAR_NEVER
	 * <p>
	 * setVerticalScrollBarPolicy set to VERTICAL_SCROLLBAR_AS_NEEDED
	 * 
	 * VerticalScrollBar UnitIncrement set to 16
	 * <p>
	 * VerticalScrollBar UI set to AlternateDarkScrollBarUI
	 * 
	 * @return JScrollPane
	 */
	private JScrollPane getScrollPane() {
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		scrollPane.getVerticalScrollBar().setUnitIncrement(16);
		scrollPane.getVerticalScrollBar().setValue(0);
		scrollPane.getVerticalScrollBar().setUI(new AlternateDarkScrollBarUI());
		return scrollPane;
	}

	/**
	 * Gets the JPanel object with each trending show added as a Show panel
	 * 
	 * @return JPanel
	 * @throws DBException
	 */
	private JPanel getTrendingShows() throws DBException {
		return prepareShowList(mediahelper.getTrendingShows(), TabType.TRENDING);
	}

	/**
	 * Gets the JPanel object with each user subscribed show added as a Show
	 * panel
	 * 
	 * @return JPanel
	 * @throws DBException
	 */
	private JPanel getSubscribedShows() throws DBException {
		return prepareShowList(mediahelper.getSubscribedShows(), TabType.SUBSCRIBED);
	}

	/**
	 * Gets the JPanel object with each upcoming episode for user subscribed
	 * shows added as a Episode panel
	 * 
	 * @return JPanel
	 * @throws DBException
	 */
	private JPanel getUpcomingEpisodes() throws DBException {
		List<Episode> episodes = mediahelper.getUpcomingEpisodes();
		return getEpisodePanels(episodes);
	}

	/**
	 * Gets the JPanel object with each recent episode for user subscribed shows
	 * added as a Episode panel
	 * 
	 * @return JPanel
	 * @throws DBException
	 */
	private JPanel getRecentEpisodes() throws DBException {
		List<Episode> episodes = mediahelper.getRecentEpisodes();
		return getEpisodePanels(episodes);
	}

	/**
	 * Gets the JPanel object with given list of episodes added as a Episode
	 * panels
	 * 
	 * @param episodes
	 * @return JPanel
	 * @throws DBException
	 */
	private JPanel getEpisodePanels(List<Episode> episodes) throws DBException {
		JPanel listContainer = new JPanel();
		listContainer.setLayout(new BoxLayout(listContainer, BoxLayout.Y_AXIS));
		for (Episode episode : episodes) {
			listContainer.add(new EpisodePanel(episode).getPanel());
		}
		return listContainer;
	}

	/**
	 * Gets the JPanel object with given list of shows and tab type adds one
	 * show summary panel for each
	 * 
	 * @param shows
	 * @param tabtype
	 * @return JPanel
	 * @throws DBException
	 */
	private JPanel prepareShowList(List<Show> shows, TabType tabtype) throws DBException {
		JPanel listContainer = new JPanel();
		listContainer.setLayout(new BoxLayout(listContainer, BoxLayout.Y_AXIS));
		for (Show show : shows) {
			listContainer.add(new ShowPanel(show, tabtype).getShowPanel());
		}
		return listContainer;
	}

	/**
	 * Gets the data from the data source and binds the user subscription list. 
	 * 
	 * @throws DBException
	 */
	public void refreshSubscribedTab() throws DBException {
		scrollSuscribed.getViewport().add(getSubscribedShows());
	}
}
