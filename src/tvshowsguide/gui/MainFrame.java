package tvshowsguide.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import tvshowsguide.Global;
import tvshowsguide.MediaHelper;
import tvshowsguide.entities.Episode;
import tvshowsguide.entities.Season;
import tvshowsguide.exceptions.DBException;
import tvshowsguide.utils.GUIUtils;
import tvshowsguide.utils.UserSession;

/**
 * Creates the applications Main frame with header containing navigation and
 * user details and home tabs containing upcoming episodes, recently aired
 * episodes and trending shows.
 *
 */
public class MainFrame extends JFrame {
	private static final long serialVersionUID = 1L;

	private Container contentPane = getContentPane();
	private JPanel mainContainer = new JPanel();

	private HomeTabPage homeTabPanel = new HomeTabPage();
	private ShowTabPage showTabPanel = new ShowTabPage();
	private JPanel headPanel = new JPanel();

	private int currView = 0;
	private JButton btnBack = Global.createButton();
	private JButton btnSearch = Global.createButton();
	private Color colorNavPanel = Global.getInstance().uicolor_primary;

	/**
	 * Create the frame and renders the content of the frame.
	 */
	public MainFrame() {
		mainContainer.setBackground(colorNavPanel);
		setFrameProperties();
		mainContainer.setLayout(new BoxLayout(mainContainer, BoxLayout.Y_AXIS));
		mainContainer.add(homeTabPanel);
		renderHeaderPanel();
		contentPane.add(headPanel, BorderLayout.NORTH);
		contentPane.add(mainContainer, BorderLayout.CENTER);
		setNotifications();
	}

	/**
	 * Sets notifications for upcoming episodes.
	 */
	private void setNotifications() {
		try {
			List<Episode> episodes = new MediaHelper().getUpcomingEpisodes();
			if (!episodes.isEmpty()) {
				new NotificationFrame(episodes.get(0)).showNotificationNow();
			}
		} catch (DBException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Sets the Frame properties like title, background color and Close
	 * operation.
	 */
	private void setFrameProperties() {
		setTitle("TV Guide");
		setBackground(colorNavPanel);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		// Removes the title bar and resize the window to full screen with
		// visible task bar.
		//
		// Dimension scrSize = Toolkit.getDefaultToolkit().getScreenSize();
		// Insets toolHeight =
		// Toolkit.getDefaultToolkit().getScreenInsets(getGraphicsConfiguration());
		// setBounds(0, 0, scrSize.width, scrSize.height - toolHeight.bottom);
		// setUndecorated(true);
	}

	/**
	 * Renders header panel with search, navigation and user panel.
	 */
	private void renderHeaderPanel() {
		btnBack.setBorder(BorderFactory.createEmptyBorder(0, 25, 0, 0));
		headPanel.setBackground(colorNavPanel);
		headPanel.setLayout(new BorderLayout(0, 0));
		btnSearch.setIcon(GUIUtils.getImageIconFromPath(Global.ICON_SEARCH, 40, 40));
		headPanel.add(btnSearch, BorderLayout.WEST);
		headPanel.add(getUserPanel(), BorderLayout.EAST);
		btnSearch.setBorder(BorderFactory.createEmptyBorder(0, 25, 0, 0));
		btnSearch.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				openSearchFrame();
			}
		});

		btnBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				toggleContainers(showTabPanel);
			}
		});
	}

	/**
	 * Creates user panel with user details.
	 * 
	 * @return JPanel
	 */
	private JPanel getUserPanel() {
		JPanel userPanel = new JPanel();
		userPanel.setBackground(colorNavPanel);
		userPanel.setLayout(new FlowLayout());
		JLabel lblUser = new JLabel(UserSession.getUserid());
		lblUser.setForeground(Color.WHITE);
		JLabel lblUserIcon = new JLabel();
		lblUserIcon.setIcon(GUIUtils.getImageIconFromPath(Global.ICON_USER, 40, 40));
		lblUserIcon.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
		userPanel.add(lblUser);
		userPanel.add(lblUserIcon);
		return userPanel;
	}

	/**
	 * Based on the current view toggles between the home tab page and show tab
	 * page and the navigation panels back and search buttons is hidden/shown.
	 * <p>
	 * If the current view of main frame is home tab page, the given show tab
	 * panel is added to the main frame and the back button is shown and search
	 * buttons is hidden.
	 * <p>
	 * If the current view of main frame is show tab page, the given home tab
	 * panel is added to the main frame and the back button is hidden and search
	 * buttons is shown.
	 * 
	 * @param showTabPanel
	 */
	public void toggleContainers(ShowTabPage showTabPanel) {
		mainContainer.removeAll();
		if (currView == 0) {
			currView = 1;
			this.showTabPanel = showTabPanel;
			mainContainer.add(showTabPanel);
			headPanel.remove(btnSearch);
			btnBack.setIcon(GUIUtils.getImageIconFromPath(Global.ICON_BACK, 40, 40));
			headPanel.add(btnBack, BorderLayout.WEST);
		} else {
			currView = 0;
			mainContainer.add(homeTabPanel);
			headPanel.remove(btnBack);
			btnSearch.setIcon(GUIUtils.getImageIconFromPath(Global.ICON_SEARCH, 40, 40));
			headPanel.add(btnSearch, BorderLayout.WEST);
		}
		headPanel.updateUI();
		mainContainer.updateUI();
	}

	/**
	 * Adds the new season tab pane for the current show in the show tab page to
	 * the show tab page
	 * 
	 * @param season
	 */
	public void addSeasonTab(Season season) {
		new SeasonTabPanel(showTabPanel, season);
		mainContainer.updateUI();
	}

	/**
	 * Opens the search frame on search button click.
	 */
	private void openSearchFrame() {
		JDialog searchframe = new SearchFrame();
		searchframe.setVisible(true);
	}

	/**
	 * Refreshes the user subscription list in the home tab page.
	 * @throws DBException
	 */
	public void refreshSubscribedTab() throws DBException {
		homeTabPanel.refreshSubscribedTab();
	}
}