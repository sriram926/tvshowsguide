package tvshowsguide.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.border.Border;

import tvshowsguide.Global;
import tvshowsguide.entities.Season;
import tvshowsguide.exceptions.DBException;
import tvshowsguide.utils.GUIUtils;

/**
 * Generates the Season JPanel based on the given season object.
 *
 */
public class SeasonPanel extends JPanel {

	private static final long serialVersionUID = 1L;
	private Season season;
	private Color border;
	private Color background;

	/**
	 * Set the season object in the SeasonsPanel
	 * 
	 * @param episode
	 */
	public SeasonPanel(Season season) {
		this.season = season;
		this.border = new Color(0, 0, 0);
		this.background = Global.getInstance().uicolor_primary;
	}

	/**
	 * Gets the Panel with Season Summary
	 * 
	 * @return JPanel with Season Summary as content
	 * @throws DBException
	 */
	public JPanel getPanel() throws DBException {
		JPanel mainContainer = new JPanel();
		mainContainer.setLayout(new FlowLayout());
		mainContainer.setBackground(background);

		Border paneEdge = BorderFactory.createLineBorder(border, 10);
		mainContainer.setBorder(paneEdge);

		JPanel picContainer = new JPanel();
		JPanel dataContainer = new JPanel();
		JPanel buttonContainer = new JPanel();
		dataContainer.setBackground(background);
		picContainer.setBackground(background);
		buttonContainer.setBackground(background);
		addData(dataContainer);
		picContainer.add(GUIUtils.getImageFromURL(season.images.getPoster().getThumb()));

		JButton showDetailsBtn = Global.createButton();
		showDetailsBtn.setIcon(GUIUtils.getImageIconFromPath(Global.ICON_DETAILS, 40, 40));

		showDetailsBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnDetailsClick();
			}
		});

		buttonContainer.setLayout(new BorderLayout());
		buttonContainer.add(showDetailsBtn, BorderLayout.SOUTH);

		mainContainer.add(picContainer);
		mainContainer.add(dataContainer);
		mainContainer.add(buttonContainer);
		return mainContainer;
	}

	/**
	 * Add season information to the dataContainer of the season Panel
	 * 
	 */
	private void addData(JPanel dataContainer) {
		JPanel titlepanel = new JPanel();
		titlepanel.setBackground(background);
		JLabel title = new JLabel(season.getName());
		titlepanel.add(title);

		JTextArea ta = new JTextArea(season.getOverview(), 6, 35);
		ta.setOpaque(false);
		ta.setLineWrap(true);
		ta.setWrapStyleWord(true);
		ta.setEditable(false);

		JPanel overviewPanel = new JPanel();
		overviewPanel.setBackground(background);
		overviewPanel.add(ta);

		dataContainer.setLayout(new BorderLayout());
		dataContainer.add(titlepanel, BorderLayout.NORTH);
		dataContainer.add(overviewPanel, BorderLayout.CENTER);
	}

	/**
	 * Creates new season tab pane in the show summary with seasons episodes.
	 * 
	 */
	private void btnDetailsClick() {
		Global.getMainFrame().addSeasonTab(season);
	}
}
