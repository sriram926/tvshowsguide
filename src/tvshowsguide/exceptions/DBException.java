package tvshowsguide.exceptions;

/**
 * Handles the database exceptions
 *
 */
public class DBException extends Exception {
	
	private static final long serialVersionUID = 1L;
	
	public DBException(String msg, Throwable cause)
	{
		super(msg,cause);
	}

}
