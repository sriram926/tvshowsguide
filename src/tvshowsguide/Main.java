package tvshowsguide;

import java.awt.EventQueue;

import javax.swing.JFrame;

import tvshowsguide.gui.Login;

/**
 * Main has the main method for the application.
 * <p>
 * Application starts with login frame and has method renderMainFrame to start
 * the application main frame.
 *
 */
public class Main {

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Login login = new Login();
					login.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Renders main frame and sets its visibility to true and sets the extended
	 * state to Maximized.
	 * 
	 * @see JFrame
	 */
	public void renderMainFrame() {
		Global.getMainFrame().setVisible(true);
		Global.getMainFrame().setExtendedState(Global.getMainFrame().getExtendedState() | JFrame.MAXIMIZED_BOTH);
	}
}
