package tvshowsguide.uitheme;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Rectangle;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.plaf.basic.BasicScrollBarUI;

import tvshowsguide.Global;

/**
 * This class is used to override the default scroll bar UI properties using the
 * Primary color for ScrollBar and Secondary color for tack background defined
 * in Global class.
 *
 */
public class DarkScrollBarUI extends BasicScrollBarUI {
	@Override
	protected JButton createDecreaseButton(int orientation) {
		return createZeroButton();
	}

	@Override
	protected void paintTrack(Graphics g, JComponent c, Rectangle trackBounds) {
		c.setBackground(Color.BLACK);
	}

	@Override
	protected JButton createIncreaseButton(int orientation) {
		return createZeroButton();
	}

	@Override
	protected void configureScrollBarColors() {
		this.thumbColor = Global.getInstance().uicolor_primary;
	}

	private JButton createZeroButton() {
		JButton jbutton = new JButton();
		jbutton.setPreferredSize(new Dimension(0, 0));
		jbutton.setMinimumSize(new Dimension(0, 0));
		jbutton.setMaximumSize(new Dimension(0, 0));
		return jbutton;
	}
}
