package tvshowsguide.uitheme;

import java.awt.Color;
import java.awt.FontMetrics;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Polygon;
import java.awt.Rectangle;
import java.util.Arrays;

import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.basic.BasicTabbedPaneUI;

import tvshowsguide.Global;

/**
 * This class is used to override the default TabbedPane UI properties using the
 * Primary color defined in Global class.
 *
 */
public class DarkTabbedPaneUI extends BasicTabbedPaneUI {
	private Color defaultcolor;
	private int tabWidth = 12;
	private int tabHeight = 18;
	private int focusWidth = 4;
	private Polygon shape;

	public static ComponentUI createUI(JComponent c) {
		return new DarkTabbedPaneUI();
	}

	@Override
	protected void installDefaults() {
		super.installDefaults();
		defaultcolor = new Color(192, 192, 192);
		tabAreaInsets.right = tabHeight;
		overrideContentBorderInsetsOfUI();
	}

	@Override
	protected void layoutLabel(int tabPlacement, FontMetrics metrics, int tabIndex, String title, Icon icon, Rectangle tabRect, Rectangle iconRect,
			Rectangle textRect, boolean isSelected) {
		Rectangle tabRectPeq = new Rectangle(tabRect);
		tabRectPeq.width -= tabWidth;
		super.layoutLabel(tabPlacement, metrics, tabIndex, title, icon, tabRectPeq, iconRect, textRect, isSelected);
	}

	@Override
	protected void paintTabArea(Graphics g, int tabPlacement, int selectedIndex) {
		if (runCount > 1) {
			int lines[] = new int[runCount];
			for (int i = 0; i < runCount; i++) {
				lines[i] = rects[tabRuns[i]].y + (tabPlacement == TOP ? maxTabHeight : 0);
			}
			Arrays.sort(lines);
			int count = runCount;
			for (int i = 0; i < lines.length - 1; i++, count--) {
				Polygon tabheadbutton = new Polygon();
				tabheadbutton.addPoint(0, lines[i]);
				tabheadbutton.addPoint(tabPane.getWidth() - 2 * count - 2, lines[i]);
				tabheadbutton.addPoint(tabPane.getWidth() - 2 * count, lines[i] + 3);
				if (i < lines.length - 2) {
					tabheadbutton.addPoint(tabPane.getWidth() - 2 * count, lines[i + 1]);
					tabheadbutton.addPoint(0, lines[i + 1]);
				} else {
					tabheadbutton.addPoint(tabPane.getWidth() - 2 * count, lines[i] + rects[selectedIndex].height);
					tabheadbutton.addPoint(0, lines[i] + rects[selectedIndex].height);
				}
				tabheadbutton.addPoint(0, lines[i]);
				g.setColor(new Color(0, 0, 0, 0));
				g.fillPolygon(tabheadbutton);
				g.setColor(darkShadow.darker());
				g.drawPolygon(tabheadbutton);
			}
		}
		super.paintTabArea(g, tabPlacement, selectedIndex);
	}

	@Override
	protected void paintTabBackground(Graphics g, int tabPlacement, int tabIndex, int x, int y, int w, int h, boolean isSelected) {
		Graphics2D g2D = (Graphics2D) g;
		GradientPaint gradientShadow;
		int xp[] = null;
		int yp[] = null;

		xp = new int[] { x, x, x + 3, x + w - tabWidth - 6, x + w - tabWidth - 2, x + w - tabWidth, x + w, x };
		yp = new int[] { y + h, y + 3, y, y, y + 1, y + 3, y + h, y + h };
		gradientShadow = new GradientPaint(0, 0, new Color(255, 255, 255), 0, y + h / 4, Global.getInstance().uicolor_secondary);
		shape = new Polygon(xp, yp, xp.length);

		if (isSelected) {
			g2D.setColor(defaultcolor);
			tabPane.setForegroundAt(tabIndex, Color.WHITE);
			g2D.setPaint(gradientShadow);
		} else {
			g2D.setColor(tabPane.getBackgroundAt(tabIndex));
			tabPane.setForegroundAt(tabIndex, Color.BLACK);
			g2D.setColor(defaultcolor);
		}
		g2D.fill(shape);
	}

	@Override
	protected int calculateTabWidth(int tabPlacement, int tabIndex, FontMetrics metrics) {
		return 8 + tabWidth + super.calculateTabWidth(tabPlacement, tabIndex, metrics);
	}

	@Override
	protected int calculateTabHeight(int tabPlacement, int tabIndex, int fontHeight) {
		if (tabPlacement == LEFT || tabPlacement == RIGHT) {
			return super.calculateTabHeight(tabPlacement, tabIndex, fontHeight);
		} else {
			return focusWidth + super.calculateTabHeight(tabPlacement, tabIndex, fontHeight);
		}
	}

	@Override
	protected void paintTabBorder(Graphics g, int tabPlacement, int tabIndex, int x, int y, int w, int h, boolean isSelected) {
	}

	@Override
	protected void paintFocusIndicator(Graphics g, int tabPlacement, Rectangle[] rects, int tabIndex, Rectangle iconRect, Rectangle textRect,
			boolean isSelected) {
		if (tabPane.hasFocus() && isSelected) {
			g.setColor(Global.getInstance().uicolor_secondary);
			g.drawPolygon(shape);
		}
	}

	@Override
	protected void paintContentBorderTopEdge(Graphics g, int tabPlacement, int selectedIndex, int x, int y, int w, int h) {
		g.setColor(Global.getInstance().uicolor_secondary);
		g.drawRect(x, y, w, 5);
	}

	@Override
	protected void paintContentBorderLeftEdge(Graphics g, int tabPlacement, int selectedIndex, int x, int y, int w, int h) {
		g.setColor(Global.getInstance().uicolor_secondary);
		g.drawRect(x, y, 5, h);
	}

	@Override
	protected void paintContentBorderBottomEdge(Graphics g, int tabPlacement, int selectedIndex, int x, int y, int w, int h) {
	}

	@Override
	protected void paintContentBorderRightEdge(Graphics g, int tabPlacement, int selectedIndex, int x, int y, int w, int h) {
	}

	public void overrideContentBorderInsetsOfUI() {
		this.contentBorderInsets.top = 1;
		this.contentBorderInsets.left = 1;
		this.contentBorderInsets.right = 0;
		this.contentBorderInsets.bottom = 0;
	}
}