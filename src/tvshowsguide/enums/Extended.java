package tvshowsguide.enums;

/**
 * For requesting different extended levels of information from the API.
 * <ul>
 * <li>Level - Description
 * <li>min - Default Returns enough info to match locally.
 * <li>images - Minimal info and all images.
 * <li>full - Complete info for an item.
 * <li>full,images - Complete info and all images.
 * </ul>
 *
 */
public enum Extended implements AppEnum {
	DEFAULT_MIN("min"), IMAGES("images"), FULL("full"), FULLIMAGES("full,images");

	private final String value;

	private Extended(String value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return value;
	}
}
