package tvshowsguide.enums;

/**
 * All methods will accept or return standard media objects for movie, show,
 * season, episode items.
 *
 */
public enum MediaType implements AppEnum {
	MOVIE("movie"), SHOW("show"), SEASON("season"), EPISODE("episode");

	private final String value;

	private MediaType(String value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return value;
	}
}
