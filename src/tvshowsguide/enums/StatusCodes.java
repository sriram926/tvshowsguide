package tvshowsguide.enums;

/**
 * Possible HTTP status codes for the API.
 * <ul>
 * <li>200 Success
 * <li>201 Success - new resource created (POST)
 * <li>204 Success - no content to return (DELETE)
 * <li>400 Bad Request - request couldn't be parsed
 * <li>401 Unauthorized - OAuth must be provided
 * <li>403 Forbidden - invalid API key or unapproved app
 * <li>404 Not Found - method exists, but no record found
 * <li>405 Method Not Found - method doesn't exist
 * <li>409 Conflict - resource already created
 * <li>412 Precondition Failed - use application/json content type
 * <li>422 Unprocessable Entity - validation errors
 * <li>429 Rate Limit Exceeded
 * <li>500 Server Error
 * <li>503 Service Unavailable - server overloaded
 * <li>520 Service Unavailable - Cloudflare error1
 * <li>521 Service Unavailable - Cloudflare error2
 * <li>522 Service Unavailable - Cloudflare error3
 * </ul>
 */
public enum StatusCodes implements AppEnum {
	SUCCESS("200"),
	SUCCESSPOST("201"),
	SUCCESSDELETE("204"),
	BADREQUEST("400"),
	UNAUTHORIZED("401"),
	FORBIDDEN("403"),
	NOTFOUND("404"),
	METHODNOTFOUND("405"),
	CONFLICT("409"),
	PRECONDFAILED("412"),
	UNPROCESSABLE("422"),
	RATELIMITEXCEEDED("429"),
	SERVERERROR("500"),
	SERVEROVERLOADED("503"),
	CLOUDFLAREERROR1("520"),
	CLOUDFLAREERROR2("521"),
	CLOUDFLAREERROR3("522");

	private final String value;

	private StatusCodes(String value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return value;
	}
}
