package tvshowsguide.enums;

/**
 * Has tab type from Home tab page.
 * <ul>
 * <li>Trending Now
 * <li>Subscribed
 * <li>Search
 *
 */
public enum TabType implements AppEnum {
	TRENDING("Trending Now"), 
	SUBSCRIBED("Subscribed"),
	SEARCH("Search");
	
	private final String value;

	private TabType(String value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return value;
	}
}
