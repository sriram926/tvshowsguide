package tvshowsguide.enums;

/**
 * All the possible API URLS for making a requests
 *
 */
public enum TraktURIs implements AppEnum {
	ACCESS_AUTHORIZE("/oauth/authorize"),
	ACCESS_TOKEN("/oauth/token"),

	LOGIN("/auth/login"),

	SYNC_LASTACTIVITIES("/sync/last_activities"),
	SYNC_PLAYBACK("/sync/playback"),
	SYNC_COLLECTIONMOVIES("/sync/collection/movies"),
	SYNC_WATCHEDMOVIES("/sync/watched/movies"),
	SYNC_RATEDMOVIES("/sync/ratings/movies"),
	SYNC_COLLECTIONEPISODES("/sync/collection/shows"),
	SYNC_WATCHEDEPISODES("/sync/watched/shows"),
	SYNC_RATEDEPISODES("/sync/ratings/episodes"),
	SYNC_RATEDSHOWS("/sync/ratings/shows"),
	SYNC_COLLECTIONADD("/sync/collection"),
	SYNC_COLLECTIONREMOVE("/sync/collection/remove"),
	SYNC_WATCHEDHISTORYADD("/sync/history"),
	SYNC_WATCHEDHISTORYREMOVE("/sync/history/remove"),
	SYNC_RATINGSADD("/sync/ratings"),
	SYNC_RATINGSREMOVE("/sync/ratings/remove"),
	SYNC_WATCHLISTADD("/sync/watchlist"),
	SYNC_WATCHLISTREMOVE("/sync/watchlist/remove"),

	USER_LISTS("/user/{0}/lists"),
	USER_LISTITEMS("/user/{0}/lists/{1}/items"),
	USER_LISTADD("/user/{0}/lists"),
	USER_LISTEDIT("/user/{0}/lists/{1}"),
	USER_LISTITEMSADD("/user/{0}/lists/{1}/items"),
	USER_LISTITEMSREMOVE("/user/{0}/lists/{1}/items/remove"),
	USER_LISTLIKE("/user/{0}/lists/{1}/like"),
	USER_WATCHLISTMOVIES("/user/{0}/watchlist/movies"),
	USER_WATCHLISTSHOWS("/user/{0}/watchlist/shows"),
	USER_WATCHLISTEPISODES("/user/{0}/watchlist/episodes"),
	USER_PROFILE("/user/{0}"),
	USER_FOLLOWERREQUESTS("/user/requests"),
	USER_STATS("/user/{0}/stats"),

	RECOMMENDED_MOVIES("/recommendations/movies"),
	RECOMMENDED_SHOWS("/recommendations/shows"),

	RELATED_MOVIES("/movies/{0}/related"),
	RELATED_SHOWS("/shows/{0}/related"),

	TRENDING_MOVIES("/movies/trending"),
	TRENDING_SHOWS("/shows/trending"),

	POPULAR_MOVIES("/movies/popular"),
	POPULAR_SHOWS("/shows/popular"),

	MOVIE_COMMENTS("/movies/{0}/comments"),
	SHOW_COMMENTS("/shows/{0}/comments"),
	SEASON_COMMENTS("/shows/{0}/seasons/{1}/comments"),
	EPISODE_COMMENTS("/shows/{0}/seasons/{1}/episodes/{2}/comments"),

	COMMENT_LIKE("/comments/{0}/like"),
	COMMENT_REPLIES("/comments/{0}/replies"),

	SEARCH("/search"),

	NETWORK_FRIENDS("/user/{0}/friends"),
	NETWORK_FOLLOWERS("/user/{0}/followers"),
	NETWORK_FOLLOWING("/user/{0}/following"),
	NETWORK_FOLLOWREQUEST("/user/requests/{0}"),
	NETWORK_FOLLOWUSER("/user/{0}/follow"),

	SHOW_SUMMARY("/shows/{0}"),
	SHOW_SEASONS("/shows/{0}/seasons"),
	MOVIE_SUMMARY("/movies/{0}"),
	EPISODE_SUMMARY("/shows/{0}/seasons/{1}/episodes/{2}"),
	PERSON_SUMMARY("/people/{0}"),

	SEASON_EPISODES("/shows/{0}/seasons/{1}"),

	CALENDAR_SHOWS("/calendar/shows/{0}/{1}"),
	CALENDAR_PREMIERES("/calendar/shows/premieres/{0}/{1}"),
	CALENDAR_NEWPREMIERES("/calendar/shows/new/{0}/{1}"),

	DISMISS_RECOMMENDED_MOVIE("/recommendations/movies/{0}"),
	DISMISS_RECOMMENDED_SHOW("/recommendations/shows/{0}"),

	DELETELIST("/user/{0}/lists/{1}");

	private final String value;

	private TraktURIs(String value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return value;
	}
}
