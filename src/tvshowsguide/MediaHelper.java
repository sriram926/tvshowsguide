package tvshowsguide;

import java.util.ArrayList;
import java.util.List;

import tvshowsguide.api.ApiHelper;
import tvshowsguide.db.DBHelper;
import tvshowsguide.entities.Episode;
import tvshowsguide.entities.Media;
import tvshowsguide.entities.Season;
import tvshowsguide.entities.Show;
import tvshowsguide.enums.Extended;
import tvshowsguide.enums.MediaType;
import tvshowsguide.exceptions.DBException;

/**
 * MediaHelper class handles the data insertions and data fetching to media
 * objects.
 * <p>
 * MediaHelper makes requests to database and API.
 * <p>
 * If data is available in local data source, it returns or else it fetches from
 * API
 * 
 */
public class MediaHelper {
	DBHelper db = new DBHelper();
	ApiHelper api = new ApiHelper();

	/**
	 * Given the show object gets its information like seasons and episodes from
	 * API to insert them into database.
	 * 
	 * @param show
	 * @see tvshowsguide.entities.Show
	 */
	private void addNewShowToDB(Show show) {
		try {
			int id = show.ids.getTrakt();
			db.insertShow(show);
			List<Season> seasons = api.getShowSeasons(Integer.toString(id), Extended.FULLIMAGES);
			for (Season season : seasons) {
				season.setShowId(show.getIds().getTrakt());
				db.insertSeason(season);

				List<Episode> episodes = api.getSeasonEpisodes(Integer.toString(id), season.getNumber().toString(), Extended.FULLIMAGES);
				for (Episode episode : episodes) {
					episode.setShowId(show.getIds().getTrakt());
					episode.setSeasonId(season.getIds().getTrakt());
					db.insertEpisode(episode);
				}
			}
		} catch (DBException e) {
			System.out.println("Unable to add data to database");
			e.printStackTrace();
		}
	}

	/**
	 * Based on the given id the Show object is returned from the database
	 * 
	 * @param Show id
	 * @return Show object
	 * @throws DBException
	 * @see tvshowsguide.entities.Show
	 */
	public Show getShowById(int id) throws DBException {
		return db.getShowById(id);
	}

	/**
	 * Based on the given id the Season object is returned from the database
	 * 
	 * @param Season id
	 * @return Season object
	 * @throws DBException
	 * @see tvshowsguide.entities.Season
	 */
	public Season getSeasonById(int id) throws DBException {
		return db.getSeasonById(id);
	}

	/**
	 * Gets the list of trending shows from the API.
	 * 
	 * @return List of Shows
	 * @throws DBException
	 * @see tvshowsguide.entities.Show
	 */
	public List<Show> getTrendingShows() {
		api.setPagination(Global.PAGINATION_TRENDING);
		return api.getTrendingShows();
	}

	/**
	 * Searches titles and descriptions of shows in the API for a given string.
	 * 
	 * @param searchFor
	 * @return List of Shows
	 * @see tvshowsguide.entities.Show
	 */
	@SuppressWarnings("unchecked")
	public List<Show> searchForShow(String searchFor) {
		api.setPagination(Global.PAGINATION_SEARCH);
		List<? super Media> media = api.getMedia(searchFor, MediaType.SHOW);
		List<? extends Media> shows = new ArrayList<Show>();
		shows = (List<? extends Media>) media;
		return (List<Show>) shows;
	}

	/**
	 * Gets the list of all season currently known for a given Show by ID from
	 * the database.
	 * 
	 * @param showid
	 * @return List of Seasons
	 * @throws DBException
	 * @see tvshowsguide.entities.Season
	 */
	public List<Season> getAllSeasonsByShowId(int showid) throws DBException {
		return db.getAllSeasonsByShowId(showid);
	}

	/**
	 * Gets the list of all episodes currently known for a given season by ID
	 * from the database.
	 * 
	 * @param seasonid
	 * @return List of Episodes
	 * @throws DBException
	 * @see tvshowsguide.entities.Episode
	 */
	public List<Episode> getAllEpisodesBySeasonId(int seasonid) throws DBException {
		return db.getAllEpisodesBySeasonId(seasonid);
	}

	/**
	 * Adds the given show to user subscription list in the database.
	 * 
	 * @param show
	 * @throws DBException
	 * @see tvshowsguide.entities.Show
	 */
	public void addSubscription(Show show) throws DBException {
		if (!db.isShowExists(show.ids.getTrakt())) {
			addNewShowToDB(show);
		}
		db.insertDeleteSubscription(show.ids.getTrakt(), true);
	}

	/**
	 * Removes the given show to user subscription list in the database.
	 * 
	 * @param id
	 * @throws DBException
	 * @see tvshowsguide.entities.Show
	 */
	public void removeSubscription(int id) throws DBException {
		db.insertDeleteSubscription(id, false);
	}

	/**
	 * Checks id given show is already in the user subscription list in the
	 * database.
	 * 
	 * @param id
	 * @throws DBException
	 * @see tvshowsguide.entities.Show
	 */
	public boolean checkSubscription(int id) throws DBException {
		return db.isSubscribedShow(id);
	}

	/**
	 * Get list of subscribed shows based on the user session.
	 * 
	 * @return List of Shows
	 * @throws DBException
	 * @see tvshowsguide.entities.Show
	 */
	public List<Show> getSubscribedShows() throws DBException {
		return db.getSubscribedShows();
	}

	/**
	 * Get list of subscribed shows upcoming episodes based on the user session.
	 * 
	 * @return List of Episodes
	 * @throws DBException
	 * @see tvshowsguide.entities.Episode
	 */
	public List<Episode> getUpcomingEpisodes() throws DBException {
		return db.getUpcomingEpisodes(Global.LOOKUPDAYS_UPCOMING);
	}

	/**
	 * Get list of subscribed shows recently air episodes based on the user
	 * session.
	 * 
	 * @return List of Episodes
	 * @throws DBException
	 * @see tvshowsguide.entities.Episode
	 */
	public List<Episode> getRecentEpisodes() throws DBException {
		return db.getRecentEpisodes(Global.LOOKUPDAYS_RECENT);
	}
}
